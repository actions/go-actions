# go-actions

This package implements primitives for operating on actions.

In simple word action is like a chained rpc.

# packages

* [`action`](https://godoc.org/github.com/crackcomm/go-actions/action) - Action and context structure
* [`cloud/endpoint`](https://godoc.org/github.com/crackcomm/go-actions/cloud/storage) - Cloud endpoints management
* [`cloud/identity`](https://godoc.org/github.com/crackcomm/go-actions/cloud/storage) - Cloud identity management
* [`cloud/identity/token`](https://godoc.org/github.com/crackcomm/go-actions/cloud/storage) - Cloud tokens management
* [`cloud/storage`](https://godoc.org/github.com/crackcomm/go-actions/cloud/storage) - Storage interface
* [`cloud/storage/etcd`](https://godoc.org/github.com/crackcomm/go-actions/cloud/storage) - Client for etcd key-value storage
* [`cloud/worker`](https://godoc.org/github.com/crackcomm/go-actions/cloud/worker) - Constructing workers
* [`core`](https://godoc.org/github.com/crackcomm/go-actions/core) - Core holds functions and actions and can execute them
* [`encoding/json`](https://godoc.org/github.com/crackcomm/go-actions/encoding/json) - JSON encoding for actions
* [`encoding/yaml`](https://godoc.org/github.com/crackcomm/go-actions/encoding/yaml) - YAML encoding for actions
* [`runner`](https://godoc.org/github.com/crackcomm/go-actions/runner) - Actions runner interface
* [`source`](https://godoc.org/github.com/crackcomm/go-actions/source) - Actions source interface
* [`source/file`](https://godoc.org/github.com/crackcomm/go-actions/source/file) - File actions source
* [`source/http`](https://godoc.org/github.com/crackcomm/go-actions/source/http) - HTTP actions source
* [`transport/client`](https://godoc.org/github.com/crackcomm/go-actions/transport/client) - Client interface
* [`transport/client/http`](https://godoc.org/github.com/crackcomm/go-actions/transport/client/http) - HTTP actions client
* [`transport/client/jsonrpc`](https://godoc.org/github.com/crackcomm/go-actions/transport/client/jsonrpc) - JSON/TCP actions client
* [`transport/server`](https://godoc.org/github.com/crackcomm/go-actions/transport/server) - Server interface
* [`transport/server/http`](https://godoc.org/github.com/crackcomm/go-actions/transport/server/http) - HTTP actions server
* [`transport/server/jsonrpc`](https://godoc.org/github.com/crackcomm/go-actions/transport/server/jsonrpc) - JSON/TCP actions server
