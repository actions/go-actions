// Package action defines Action structure.
//
// Action is a simple structure holding at most 3 parameters - name, ctx (context) and next action.
//
// Action name is a name of the function to call with arguments set
// then it's repeated on next action with result of execution.
//
package action

// Action - Action structure
type Action struct {
	Name string  `json:"name,omitempty"`
	Ctx  Map     `json:"ctx,omitempty"`
	Next *Action `json:"next,omitempty"`
}

// New - Creates a new action.
func New(name string, contexts ...Map) *Action {
	return &Action{
		Name: name,
		Ctx:  Flat(contexts...),
	}
}

// AddNext - Adds next action on the end.
func (a *Action) AddNext(n *Action) {
	if a.Next == nil {
		a.Next = n
		return
	}

	a.Next.AddNext(n)
	return
}

// Wrap - Wraps action around another one.
func (a *Action) Wrap(n *Action) (result *Action) {
	result = n.Clone()
	result.AddNext(a)
	return
}

// Clone - Clones action.
func (a *Action) Clone() (clone *Action) {
	clone = &Action{Name: a.Name}
	// Clone next action if not empty
	if a.Next != nil {
		clone.Next = a.Next.Clone()
	}
	// Clone action context if not empty
	if !a.Ctx.Empty() {
		clone.Ctx = a.Ctx.Clone()
	}
	return
}
