package action

import "bytes"
import "strings"
import "io/ioutil"
import . "testing"
import . "github.com/smartystreets/goconvey/convey"

func TestFormatBytes(t *T) {
	Convey("Format.Bytes()", t, func() {
		s := "some bytes"
		val := Format{[]byte(s)}
		Convey("It should return byte array", func() {
			// Check if value matches
			test, ok := val.Bytes()
			ShouldBeTrue(ok)
			So(string(test), ShouldEqual, s)
		})
	})
}

func TestFormatStringList(t *T) {
	Convey("Format.StringList()", t, func() {
		Convey("It should return the same list", func() {
			l := []string{"a", "2"}
			val := Format{l}
			l2, ok := val.StringList()

			// Check
			So(len(l2), ShouldEqual, len(l))
			So(l2[0], ShouldEqual, "a")
			So(l2[1], ShouldEqual, "2")
			ShouldBeTrue(ok)
		})
		Convey("It should convert and return string list", func() {
			l := []interface{}{"a", "2"}
			val := Format{l}
			l2, ok := val.StringList()

			// Check
			So(len(l2), ShouldEqual, len(l))
			So(l2[0], ShouldEqual, "a")
			So(l2[1], ShouldEqual, "2")
			ShouldBeTrue(ok)
		})
	})
}

func TestFormatString(t *T) {
	Convey("Format.String()", t, func() {
		Convey("It should return string", func() {
			test, ok := Format{"some string"}.String()
			ShouldBeTrue(ok)
			So(test, ShouldEqual, "some string")
		})
		Convey("It should return string from byte array", func() {
			test, ok := Format{[]byte("some string")}.String()
			ShouldBeTrue(ok)
			So(test, ShouldEqual, "some string")
		})
	})
}

func TestFormatInt(t *T) {
	Convey("Format.Int()", t, func() {
		Convey("It should return int", func() {
			test, ok := Format{31}.Int()
			ShouldBeTrue(ok)
			So(test, ShouldEqual, 31)
		})
		Convey("It should return int from float32", func() {
			var val float32 = 31
			test, ok := Format{val}.Int()
			ShouldBeTrue(ok)
			So(test, ShouldEqual, 31)
		})
		Convey("It should return int from float64", func() {
			var val float64 = 31
			test, ok := Format{val}.Int()
			ShouldBeTrue(ok)
			So(test, ShouldEqual, 31)
		})
		Convey("It should return int from int8", func() {
			var val int8 = 31
			test, ok := Format{val}.Int()
			ShouldBeTrue(ok)
			So(test, ShouldEqual, 31)
		})
		Convey("It should return int from int16", func() {
			var val int16 = 31
			test, ok := Format{val}.Int()
			ShouldBeTrue(ok)
			So(test, ShouldEqual, 31)
		})
		Convey("It should return int from int32", func() {
			var val int32 = 31
			test, ok := Format{val}.Int()
			ShouldBeTrue(ok)
			So(test, ShouldEqual, 31)
		})
		Convey("It should return int from int64", func() {
			var val int64 = 31
			test, ok := Format{val}.Int()
			ShouldBeTrue(ok)
			So(test, ShouldEqual, 31)
		})
		Convey("It should return int from uint", func() {
			var val uint = 31
			test, ok := Format{val}.Int()
			ShouldBeTrue(ok)
			So(test, ShouldEqual, 31)
		})
		Convey("It should return int from uint8", func() {
			var val uint8 = 31
			test, ok := Format{val}.Int()
			ShouldBeTrue(ok)
			So(test, ShouldEqual, 31)
		})
		Convey("It should return int from uint16", func() {
			var val uint16 = 31
			test, ok := Format{val}.Int()
			ShouldBeTrue(ok)
			So(test, ShouldEqual, 31)
		})
		Convey("It should return int from uint32", func() {
			var val uint32 = 31
			test, ok := Format{val}.Int()
			ShouldBeTrue(ok)
			So(test, ShouldEqual, 31)
		})
		Convey("It should return int from uint64", func() {
			var val uint64 = 31
			test, ok := Format{val}.Int()
			ShouldBeTrue(ok)
			So(test, ShouldEqual, 31)
		})
		Convey("It should return int from string", func() {
			test, ok := Format{"31"}.Int()
			ShouldBeTrue(ok)
			So(test, ShouldEqual, 31)
		})
		Convey("It should not return int from other value", func() {
			test, ok := Format{[]interface{}{0}}.Int()
			So(ok, ShouldEqual, false)
			So(test, ShouldEqual, 0)
		})
	})
}

func TestFormatMap(t *T) {
	Convey("Format.Map()", t, func() {
		Convey("It should format string-interface map", func() {
			m := map[string]interface{}{"ok": true}
			val := Format{m}
			m2, ok := val.Map()

			// Check
			ShouldBeTrue(ok)
			So(m2, ShouldNotEqual, nil)

			// Check map key
			So(m2["ok"], ShouldEqual, true)
		})
		Convey("It should format interface-interface map", func() {
			m := map[interface{}]interface{}{123: true}
			val := Format{m}
			m2, ok := val.Map()

			// Check
			ShouldBeTrue(ok)
			So(m2, ShouldNotEqual, nil)

			// Check map key
			So(m2["123"], ShouldEqual, true)
		})
		Convey("It should not panic on other value", func() {
			m, ok := Format{true}.Map()
			So(m, ShouldEqual, nil)
			So(ok, ShouldEqual, false)
		})
	})
}

func TestFormatIsNil(t *T) {
	Convey("Format.IsNil()", t, func() {
		t := Format{nil}
		n := Format{false}
		Convey("It should report nil", func() {
			So(t.IsNil(), ShouldEqual, true)
		})
		Convey("It should not report nil", func() {
			So(n.IsNil(), ShouldNotEqual, true)
		})
	})
}

func TestFormatReader(t *T) {
	Convey("Format.Reader()", t, func() {
		Convey("It should just cast and return", func() {
			buffer := new(bytes.Buffer)
			rc := ioutil.NopCloser(buffer)
			f := Format{rc}

			// Check
			rc2, ok := f.Reader()
			So(rc2, ShouldNotEqual, nil)
			ShouldBeTrue(rc2 == rc)
			ShouldBeTrue(ok)
		})

		Convey("It should cast and return nop closer reader", func() {
			buffer := new(bytes.Buffer)
			f := Format{buffer}

			// Check
			rc, ok := f.Reader()
			So(rc, ShouldNotEqual, nil)
			ShouldBeTrue(rc == ioutil.NopCloser(buffer))
			ShouldBeTrue(ok)
			So(rc.Close(), ShouldEqual, nil)
		})

		Convey("It should create nop closer reader from string", func() {
			buffer := "somestring"
			f := Format{buffer}

			// Check
			rc, ok := f.Reader()
			So(rc, ShouldNotEqual, nil)
			ShouldBeTrue(rc == ioutil.NopCloser(strings.NewReader(buffer)))
			ShouldBeTrue(ok)
			So(rc.Close(), ShouldEqual, nil)
		})

		Convey("It should create nop closer reader from byte array", func() {
			buffer := []byte("somestring")
			f := Format{buffer}

			// Check
			rc, ok := f.Reader()
			So(rc, ShouldNotEqual, nil)
			ShouldBeTrue(rc == ioutil.NopCloser(bytes.NewBuffer(buffer)))
			ShouldBeTrue(ok)
			So(rc.Close(), ShouldEqual, nil)
		})

		Convey("It should not panic on other value", func() {
			r, ok := Format{true}.Reader()
			So(r, ShouldEqual, nil)
			So(ok, ShouldEqual, false)
		})
	})
}
