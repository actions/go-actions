package action

import . "testing"
import . "github.com/smartystreets/goconvey/convey"

func TestNew(t *T) {
	Convey("New()", t, func() {
		Convey("Should create action with only name", func() {
			a := New("test")
			So(a.Name, ShouldEqual, "test")
			So(a.Ctx, ShouldEqual, nil)
		})
		Convey("Should create action with flattened context", func() {
			a := New("test", Map{"a": false}, Map{"a": true})
			So(a.Name, ShouldEqual, "test")
			So(a.Ctx["a"], ShouldEqual, true)
		})
	})
}

func TestActionAddNext(t *T) {
	Convey("Action.AddNext()", t, func() {
		other := &Action{
			Name: "other",
		}
		a := &Action{
			Name: "test",
			Next: other,
		}
		single := &Action{Name: "single"}
		Convey("Should add next action", func() {
			a.AddNext(single)
			So(a.Next, ShouldEqual, other)
			So(a.Next.Next, ShouldEqual, single)
		})
	})
}

func TestActionClone(t *T) {
	Convey("Action.Clone()", t, func() {
		a := &Action{
			Name: "test",
			Next: &Action{
				Name: "other",
			},
		}
		Convey("Should clone action", func() {
			b := a.Clone()
			So(a, ShouldNotEqual, b)
			So(a.Name, ShouldEqual, b.Name)
			So(b.Next, ShouldNotEqual, nil)
			So(a.Next, ShouldNotEqual, b.Next)
			So(a.Next.Name, ShouldEqual, b.Next.Name)
		})
	})
}

func TestActionWrap(t *T) {
	Convey("Action.Wrap()", t, func() {
		a := &Action{
			Name: "test",
			Ctx:  Map{"test2": true},
		}
		b := &Action{
			Name: "other",
			Ctx:  Map{"test1": true},
		}
		Convey("Should add next action", func() {
			result := a.Wrap(b)

			// check old ones
			So(a.Name, ShouldEqual, "test")
			So(len(a.Ctx), ShouldEqual, 1)
			So(a.Next, ShouldEqual, nil)
			So(b.Name, ShouldEqual, "other")
			So(len(b.Ctx), ShouldEqual, 1)
			So(b.Next, ShouldEqual, nil)

			// Check new one
			So(result.Name, ShouldEqual, "other")
			So(len(result.Ctx), ShouldEqual, 1)
			So(result.Next.Name, ShouldEqual, "test")
			So(len(result.Next.Ctx), ShouldEqual, 1)
		})
	})
}
