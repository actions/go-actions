package action

import "errors"
import "strings"
import . "testing"
import . "github.com/smartystreets/goconvey/convey"

var jsonMap = `{"num":123,"other":true,"some":"test"}`
var prettyMap = `{
  "num": 123,
  "other": true,
  "some": "test",
  "zonk": "zonkzonk"
}`

var testErr = errors.New("some test error")

func TestFlat(t *T) {
	Convey("Flat()", t, func() {
		Convey("It should flat maps", func() {
			m1 := Map{"some": "test", "other": true}
			m2 := Map{"some": "test2", "other": false, "big": true}
			expect := Map{"some": "test2", "other": false, "big": true}
			res := Flat(m1, m2)
			So(len(res), ShouldEqual, len(expect))
			So(res["some"], ShouldEqual, expect["some"])
			So(res["other"], ShouldEqual, expect["other"])
			So(res["big"], ShouldEqual, expect["big"])
		})
		Convey("It should flat empty map", func() {
			m1 := Map{"some": "test", "other": true}
			m2 := Map{"some": "test2", "other": false, "big": true}
			expect := Map{"some": "test2", "other": false, "big": true}
			res := Flat(m1, m2, nil)
			So(len(res), ShouldEqual, len(expect))
			So(res["some"], ShouldEqual, expect["some"])
			So(res["other"], ShouldEqual, expect["other"])
			So(res["big"], ShouldEqual, expect["big"])
		})
	})
}

func TestMapPop(t *T) {
	Convey("Map.Pop()", t, func() {
		m := Map{"some": "test", "other": true}
		Convey("It should pop value", func() {
			// Pop some value
			val := m.Pop("some")

			// Value should not be nil
			So(val.IsNil(), ShouldNotEqual, true)

			// Check if value matches
			test, ok := val.String()
			So(ok, ShouldEqual, true)
			So(test, ShouldEqual, "test")
		})
		Convey("It should not pop this value again", func() {
			So(m.Pop("some").IsNil(), ShouldEqual, true)
			So(len(m), ShouldEqual, 1)
		})
	})
}

func TestMapAdd(t *T) {
	Convey("Map.Add()", t, func() {
		m := Map{}
		Convey("It should add value", func() {
			// Add some value
			m.Add("some", "test")

			// Check if value matches
			some, ok := m.Get("some").String()
			So(ok, ShouldEqual, true)
			So(some, ShouldEqual, "test")
		})
		Convey("It should replace value", func() {
			// Add some value
			m.Add("some", "value")

			// Check if value has been replaced
			some, ok := m.Get("some").String()
			So(ok, ShouldEqual, true)
			So(some, ShouldEqual, "value")
		})
	})
}

func TestMapClone(t *T) {
	Convey("Map.Clone()", t, func() {
		m := Map{"a": "b", "1": 2}
		Convey("It should clone map", func() {
			m2 := m.Clone()

			So(len(m), ShouldEqual, len(m2))
			So(m["a"], ShouldEqual, m2["a"])
			So(m["1"], ShouldEqual, m2["1"])
		})
	})
}

func TestMapDefaults(t *T) {
	Convey("Map.Defaults()", t, func() {
		m := Map{"a": "b", "1": 2}
		Convey("It should fill map defaults", func() {
			m.Defaults(Map{"a": "-", "2": 3})

			So(len(m), ShouldEqual, 3)
			So(m["a"], ShouldEqual, "b")
			So(m["1"], ShouldEqual, 2)
			So(m["2"], ShouldEqual, 3)
		})
	})
}

func TestMapGet(t *T) {
	Convey("Map.Get()", t, func() {
		m := Map{"some": "test", "other": true}
		Convey("It should return value", func() {
			some, ok := m.Get("some").String()
			So(ok, ShouldEqual, true)
			So(some, ShouldEqual, "test")
		})
		Convey("It should return value again", func() {
			some, ok := m.Get("some").String()
			So(ok, ShouldEqual, true)
			So(some, ShouldEqual, "test")
		})
	})
}

func TestMapPush(t *T) {
	Convey("Map.Push()", t, func() {
		one := Map{"some": "test"}
		Convey("It should not push to empty key", func() {
			one.Push("", "im good")
			So(len(one), ShouldEqual, 1)
			So(one["some"], ShouldEqual, "test")
		})
		Convey("It should push nested value", func() {
			// Push value deep into a map
			one.Push("one.two.three", "im good")

			// Get map `one`
			o, ok := one.Get("one").Map()
			So(ok, ShouldEqual, true)

			// Get map `two`
			w, ok := o.Get("two").Map()
			So(ok, ShouldEqual, true)

			// Get value under `three`
			val, ok := w["three"].(string)
			So(ok, ShouldEqual, true)

			// Check if value matches
			So(val, ShouldEqual, "im good")
		})
	})
}

func TestMapPull(t *T) {
	Convey("Map.Pull()", t, func() {
		one := Map{"some": "test"}
		Convey("It should pull nested value", func() {
			one.Push("one.two.three", "im good")

			// Check
			good, ok := one.Pull("one.two.three").String()
			So(ok, ShouldEqual, true)
			So(good, ShouldEqual, "im good")
		})

		Convey("It should not pull non existent value", func() {
			So(one.Pull("one.two.four").IsNil(), ShouldEqual, true)
		})

		Convey("It should not pull if parent value is not a map", func() {
			So(one.Pull("some.false").IsNil(), ShouldEqual, true)
		})
	})
}

func TestMapMerge(t *T) {
	Convey("Map.Merge()", t, func() {
		one := Map{"some": "test", "a": Map{"a": 1}}
		two := Map{"other": true, "a": Map{"b": 2}, "b": Map{"a": 1}}
		res := Map{"some": "test", "other": true, "a": Map{"a": 1, "b": 2}, "b": Map{"a": 1}}
		Convey("It should deep merge two maps", func() {
			// Merge map two into one
			three := one.Merge(two)

			// Check length of merged map
			So(one, ShouldEqual, three)
			So(len(one), ShouldEqual, len(res))

			// Check if value matches
			other, ok := one.Get("other").Bool()
			So(ok, ShouldEqual, true)
			So(other, ShouldEqual, true)

			// Check deep map
			b, ok := one.Get("b").Map()
			So(ok, ShouldEqual, true)
			So(len(b), ShouldEqual, 1)
			So(b["a"], ShouldEqual, 1)

			// Check deep map
			a, ok := one.Get("a").Map()
			So(ok, ShouldEqual, true)
			So(a["a"], ShouldEqual, 1)
			So(a["b"], ShouldEqual, 2)
		})
	})
}

func TestMapKeys(t *T) {
	Convey("Map.Keys()", t, func() {
		m := Map{"some": "test", "other": true}
		Convey("It should return map keys", func() {
			keys := m.Keys()

			// Check
			So(len(keys), ShouldEqual, len(m))
		})
	})
}

func TestMapValue(t *T) {
	Convey("Map.Value()", t, func() {
		p := Map{}
		m := Map{"some": &p}
		Convey("It should return value", func() {
			So(&p, ShouldEqual, m.Value("some"))
		})
	})
}

func TestMapDelete(t *T) {
	Convey("Map.Delete()", t, func() {
		m := Map{"some": true}
		Convey("It should delete value", func() {
			m.Delete("some")
			So(m["some"], ShouldEqual, nil)
			So(len(m), ShouldEqual, 0)
		})
	})
}

func TestMapTransform(t *T) {
	Convey("Map.Keys()", t, func() {
		stripTransform := func(in Format) interface{} {
			value, _ := in.String()
			return strings.TrimSpace(value)
		}
		Convey("It should ignore unexpected keys", func() {
			m := Map{"some": "   test     ", "other": "   test    "}
			m.Transform(false, stripTransform)
			m.Transform(123, stripTransform)
		})
		Convey("It should not panic", func() {
			m := Map{"some": "   test     ", "other": "   test    "}
			m.Transform(nil, stripTransform)
		})
		Convey("It should transform one value", func() {
			m := Map{"some": "   test     ", "other": "   test    "}
			m.Transform("some", stripTransform)
			So(m["some"], ShouldEqual, "test")
		})
		Convey("It should transform all values from the list", func() {
			m := Map{"some": "   test     ", "other": "   test    "}
			m.Transform([]string{"some", "other"}, stripTransform)
			So(m["some"], ShouldEqual, "test")
			So(m["other"], ShouldEqual, "test")
		})
		Convey("It should transform all values from the map", func() {
			some := "   test     "
			m := Map{"some": "   test     ", "other": "   test     "}
			m.Transform(Map{"some": "some2", "other": "other2"}, stripTransform)
			So(m["some"], ShouldEqual, some)
			So(m["some2"], ShouldEqual, "test")
			So(m["other"], ShouldEqual, some)
			So(m["other2"], ShouldEqual, "test")
		})
	})
}

func TestMapJSON(t *T) {
	Convey("Map.JSON()", t, func() {
		m := Map{"some": "test", "other": true, "num": 123}
		Convey("It should return JSON marshaled map", func() {
			body, err := m.JSON()
			So(err, ShouldEqual, nil)
			So(string(body), ShouldEqual, jsonMap)
		})
	})
}

func TestMapPrettyJSON(t *T) {
	Convey("Map.PrettyJSON()", t, func() {
		m := Map{"some": "test", "other": true, "num": 123, "zonk": []byte(`zonkzonk`)}
		Convey("It should return pretty JSON marshaled map", func() {
			body, err := m.PrettyJSON()
			So(err, ShouldEqual, nil)
			So(string(body), ShouldEqual, prettyMap)
		})
	})
}

type onecloser struct {
	closed bool
}

func (cc *onecloser) Close() error {
	if cc.closed {
		return testErr
	}
	cc.closed = true
	return nil
}

func TestMapClose(t *T) {
	Convey("Map.Close()", t, func() {
		m := Map{"file": &onecloser{}, "other": &onecloser{}, "some": "other", "values": 123}
		Convey("It should close all values that can be closed", func() {
			m.Close()
			So(m["some"], ShouldEqual, "other")
			So(m["values"], ShouldEqual, 123)
			So(m["file"], ShouldNotEqual, nil)
			So(m["other"], ShouldNotEqual, nil)
			So(m["file"].(closer).Close(), ShouldEqual, testErr)
			So(m["other"].(closer).Close(), ShouldEqual, testErr)
		})
	})
}

func TestMapEmpty(t *T) {
	Convey("Map.Empty()", t, func() {
		Convey("It should return true when map is nil", func() {
			var m Map
			empty := m.Empty()
			So(empty, ShouldEqual, true)
		})
		Convey("It should return true when map contains no elements", func() {
			m := Map{}
			empty := m.Empty()
			So(empty, ShouldEqual, true)
		})
	})
}
