package action

import . "testing"
import . "github.com/smartystreets/goconvey/convey"

func TestActionsGet(t *T) {
	Convey("Actions.Get()", t, func() {
		l := NewActions(&Action{Name: "test"})
		Convey("It should get action", func() {
			t, err := l.Get("test")
			So(err, ShouldEqual, nil)
			So(t.Name, ShouldEqual, "test")
		})
	})
}

func TestActionsAdd(t *T) {
	Convey("Actions.Add()", t, func() {
		l := NewActions()
		Convey("It should add action", func() {
			l.Add("other", &Action{Name: "test"})
			t, err := l.Get("other")
			So(err, ShouldEqual, nil)
			So(t.Name, ShouldEqual, "test")
		})
	})
}

func TestActionsAll(t *T) {
	Convey("Actions.All()", t, func() {
		l := NewActions(&Action{Name: "test"})
		Convey("It should return all actions", func() {
			t, err := l.All()
			So(err, ShouldEqual, nil)
			So(len(t), ShouldEqual, 1)
		})
	})
}

func TestActionsFlush(t *T) {
	Convey("Actions.Flush()", t, func() {
		l := NewActions(&Action{Name: "test"}, &Action{Name: "test"}, &Action{Name: "test"}, &Action{Name: "test"})
		Convey("It should flush actions", func() {
			l.Flush()
			So(l.Len(), ShouldEqual, 0)
		})
	})
}

func TestActionsClone(t *T) {
	Convey("Actions.Clone()", t, func() {
		a := &Action{
			Name: "test",
			Next: &Action{
				Name: "other",
			},
		}
		l := NewActions(a)
		Convey("Should clone actions", func() {
			b := l.Clone()
			So(b, ShouldNotEqual, l)
			So(b.Len(), ShouldEqual, l.Len())
		})
	})
}

func TestActionsDelete(t *T) {
	Convey("Actions.Delete()", t, func() {
		l := NewActions(&Action{Name: "test"})
		Convey("It should delete action", func() {
			err := l.Delete("test")
			So(err, ShouldEqual, nil)
			So(l.Len(), ShouldEqual, 0)
		})
	})
}

func TestActionsLen(t *T) {
	Convey("Actions.Len()", t, func() {
		l := NewActions(&Action{Name: "test"}, &Action{Name: "test2"})
		Convey("It should count 2", func() {
			So(l.Len(), ShouldEqual, 2)
		})
	})
}
