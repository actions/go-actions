package action

import "strings"
import "encoding/json"

// Map - Map designed to retrieve formatted values.
type Map map[string]interface{}

// Transformer - Value transformer interface.
type Transformer func(Format) interface{}

// closer - Closer interface.
type closer interface {
	Close() error
}

// Flat - Flats all maps into a one. Last map will be at the top.
func Flat(maps ...Map) (result Map) {
	if len(maps) == 0 {
		return
	} else if len(maps) == 1 {
		return maps[0]
	} else if len(maps) > 1 {
		for _, value := range maps {
			if result == nil {
				result = value
			} else {
				result.Merge(value)
			}
		}
	}
	return
}

// Close - Closes all values in the map that can be closed ignoring all errors.
func (m Map) Close() error {
	for _, value := range m {
		if c, ok := value.(closer); ok {
			c.Close()
		}
	}
	return nil
}

// Defaults - Fills values from another map when doesnt occur in current. Returns current (changed) map.
func (m Map) Defaults(defaults Map) Map {
	for key, value := range defaults {
		if m[key] == nil {
			m[key] = value
		}
	}
	return m
}

// Clone - Makes a clone of a map.
func (m Map) Clone() (clone Map) {
	clone = make(Map)
	for key, value := range m {
		clone[key] = value
	}
	return
}

// Pop - Gets a value from map and deletes it.
func (m Map) Pop(key string) Format {
	value := m.Get(key)
	delete(m, key)
	return value
}

// Empty - Returns true when map is empty.
func (m Map) Empty() bool {
	return m == nil || len(m) == 0
}

// Add - Adds value under specified key.
func (m Map) Add(key string, value interface{}) {
	m[key] = value
}

// Get - Gets value from map and returns in Format structure.
func (m Map) Get(key string) Format {
	return Format{m[key]}
}

// Delete - Deletes value from map.
func (m Map) Delete(key string) {
	delete(m, key)
}

// Value - Returns value from map.
func (m Map) Value(key string) interface{} {
	return m[key]
}

// Pull - Gets deep nested value from dot separted key.
func (m Map) Pull(key string) Format {
	keys := strings.Split(key, ".")
	return m.pull(keys)
}

// pull - Gets deep nested value from dot separted key.
func (m Map) pull(keys []string) Format {
	// If there was only one key - return value
	if len(keys) == 1 {
		return m.Get(keys[0])
	}

	// Get map by first key
	value, ok := m.Get(keys[0]).Map()

	// If map was not found - return nil
	if !ok {
		return Format{nil}
	}

	// Continue
	return value.pull(keys[1:])
}

// Push - Pushes value under specified key. Dot-separated key is used to set nested values.
func (m Map) Push(key string, value interface{}) {
	keys := strings.Split(key, ".")
	m.push(keys, value)
}

// push - Pushes value under specified key. List key is used to set nested values.
func (m Map) push(keys []string, value interface{}) {
	// First key (key of value in `m` map)
	first := keys[0]
	if first == "" {
		return
	}

	// Remove it from the list
	keys = keys[1:]

	// If there was only one key - apply and return
	if len(keys) == 0 {
		m.Add(first, value)
		return
	}

	// If there are more keys, set nested value
	f, ok := m.Get(first).Map()

	// If a Map was not found - create a new one
	if !ok {
		f = Map{}
	}

	// Push value to nested map
	f.push(keys, value)

	// Add this map under first key to current map
	m.Add(first, f)
	return
}

// Merge - Deep merges map.
func (m Map) Merge(merge Map) Map {
	for key, value := range merge {
		if setmap, ok := merge.Get(key).Map(); ok {
			if hasmap, ok := m.Get(key).Map(); ok {
				m[key] = hasmap.Merge(setmap)
			} else {
				m[key] = setmap
			}
		} else {
			m[key] = value
		}
	}
	return m
}

// Keys - List of all keys in map.
func (m Map) Keys() (keys []string) {
	keys = []string{}
	for k := range m {
		keys = append(keys, k)
	}
	return
}

// JSON - Marshals map to JSON.
func (m Map) JSON() ([]byte, error) {
	return json.Marshal(m)
}

// PrettyJSON - Marshals map to pretty JSON.
func (m Map) PrettyJSON() ([]byte, error) {
	m.Transform(m.Keys(), bytesToString)
	return json.MarshalIndent(m, "", "  ")
}

// Transform - Transforms map.
func (m Map) Transform(keys interface{}, transformer Transformer) {
	if keys == nil {
		return
	}

	// If it's not a Format create new
	k, ok := keys.(Format)
	if !ok {
		k = Format{keys}
	}

	// If key is a string transform one key
	if key, ok := k.String(); ok {
		value := m.Get(key)
		m[key] = transformer(value)
		return
	}

	// If key is a list of stringsg transform all of them
	if keylist, ok := k.StringList(); ok {
		for _, key := range keylist {
			value := m.Get(key)
			m[key] = transformer(value)
		}
		return
	}

	// If key is a map transform all of them and write under keys represented by map-values
	if keymap, ok := k.Map(); ok {
		for key := range keymap {
			if name, ok := keymap.Get(key).String(); ok {
				value := m.Get(key)
				m[name] = transformer(value)
			}
		}
		return
	}

	return
}

// bytesToString - Tranforms byte array into string.
func bytesToString(value Format) interface{} {
	if bytearray, ok := value.Bytes(); ok {
		return string(bytearray)
	}
	return value.Interface()
}
