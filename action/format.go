package action

import "io"
import "fmt"
import "bytes"
import "reflect"
import "strconv"
import "strings"
import "io/ioutil"

// Format - Helper for formatting underlying Value.
type Format struct {
	Value interface{}
}

// Bool - Asserts a value to a bool.
func (f Format) Bool() (bool, bool) {
	value, ok := f.Value.(bool)
	return value, ok
}

// Bytes - Asserts a value to a byte array.
func (f Format) Bytes() ([]byte, bool) {
	value, ok := f.Value.([]byte)
	return value, ok
}

// String - Asserts a value to a string.
func (f Format) String() (string, bool) {
	switch f.Value.(type) {
	case string:
		return f.Value.(string), true
	case []byte:
		return string(f.Value.([]byte)), true
	case float32, float64, int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64:
		return fmt.Sprintf("%v", f.Value), true
	}
	return "", false
}

// Int - Asserts a value to int. Also parses a string value to an int.
func (f Format) Int() (int, bool) {
	switch f.Value.(type) {
	case int:
		return f.Value.(int), true
	case float32, float64:
		return int(reflect.ValueOf(f.Value).Float()), true
	case int8, int16, int32, int64:
		return int(reflect.ValueOf(f.Value).Int()), true
	case uint, uint8, uint16, uint32, uint64:
		return int(reflect.ValueOf(f.Value).Uint()), true
	case string:
		if value, err := strconv.Atoi(f.Value.(string)); err == nil {
			return value, true
		}
	}
	return 0, false
}

// Map - Converts value to Map.
func (f Format) Map() (Map, bool) {
	switch f.Value.(type) {
	case Map:
		return f.Value.(Map), true
	case map[string]interface{}:
		return Map(f.Value.(map[string]interface{})), true
	case map[interface{}]interface{}:
		res := make(Map)
		for name, value := range f.Value.(map[interface{}]interface{}) {
			// Format key to a string
			key, ok := name.(string)
			if !ok {
				key = fmt.Sprintf("%v", name)
			}
			// Add value to map
			res[key] = value
		}
		return res, true
	}
	return nil, false
}

// StringList - Asserts a value to a string list.
func (f Format) StringList() ([]string, bool) {
	if value, ok := f.Value.([]string); ok {
		return value, ok
	}
	if value, ok := f.List(); ok {
		result := []string{}
		for _, k := range value {
			f := Format{k}
			if val, ok := f.String(); ok {
				result = append(result, val)
			}
		}
		return result, true
	}
	return nil, false
}

// List - Asserts a value to a list.
func (f Format) List() ([]interface{}, bool) {
	value, ok := f.Value.([]interface{})
	return value, ok
}

// Reader - Asserts a value to a ReadCloser.
func (f Format) Reader() (rc io.ReadCloser, ok bool) {
	var r io.Reader
	switch f.Value.(type) {
	case io.ReadCloser:
		rc, ok = f.Value.(io.ReadCloser)
		return
	case io.Reader:
		r, ok = f.Value.(io.Reader)
	case string:
		var value string
		value, ok = f.Value.(string)
		r = strings.NewReader(value)
	case []byte:
		var value []byte
		value, ok = f.Value.([]byte)
		r = bytes.NewReader(value)
	}

	// If reader was set wrap to a NopCloser
	if ok && r != nil {
		rc = ioutil.NopCloser(r)
	}

	return
}

// Interface - Returns underlying value.
func (f Format) Interface() interface{} {
	return f.Value
}

// IsNil - Verifies either value is nil.
func (f Format) IsNil() bool {
	return f.Value == nil
}
