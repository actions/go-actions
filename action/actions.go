package action

// Actions - Actions map.
type Actions map[string]*Action

// NewActions - Creates new action list.
func NewActions(actions ...*Action) (result Actions) {
	result = make(Actions)
	for _, a := range actions {
		result[a.Name] = a
	}
	return
}

// Add - Adds action to the list.
func (actions Actions) Add(name string, action *Action) error {
	actions[name] = action
	return nil
}

// All - Returns all actions.
func (actions Actions) All() (Actions, error) {
	return actions, nil
}

// Get - Gets action from list by name.
func (actions Actions) Get(name string) (*Action, error) {
	return actions[name], nil
}

// Delete - Deletes action from the list by name.
func (actions Actions) Delete(name string) error {
	delete(actions, name)
	return nil
}

// Flush - Removes all actions.
func (actions Actions) Flush() {
	for key := range actions {
		delete(actions, key)
	}
}

// Clone - Clones actions.
func (actions Actions) Clone() (clone Actions) {
	clone = make(Actions)
	for name, value := range actions {
		clone[name] = value.Clone()
	}
	return
}

// Len - Returns length of actions list.
func (actions Actions) Len() int {
	return len(actions)
}
