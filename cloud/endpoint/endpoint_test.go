package endpoint

import . "testing"
import . "github.com/smartystreets/goconvey/convey"

func TestEndpoint(t *T) {
	Convey("Endpoint", t, func() {
		Convey("URL()", func() {
			endpoint := New("https://123.123.123.123")
			So(endpoint.Addr, ShouldEqual, "123.123.123.123")
			So(endpoint.Transport, ShouldEqual, "https")
			So(endpoint.URL(), ShouldEqual, "https://123.123.123.123")
		})
		Convey("Watch()", func() {
			Convey("It should watch on endpoint", func() {
				endpoint := New("http://123.123.123.123")
				So(endpoint.Addr, ShouldEqual, "123.123.123.123")
				So(endpoint.Transport, ShouldEqual, "http")
				So(endpoint.Health, ShouldEqual, true)
				go func() {
					for {
						if endpoint.watching {
							endpoint.Dead()
							break
						}
					}
				}()
				health := <-endpoint.Watch()
				So(health, ShouldEqual, false)
				So(endpoint.Health, ShouldEqual, false)
				endpoint.Stop()
			})
		})
	})
}
