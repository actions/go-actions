// Package endpoint provides abstracts for managing authorizated endpoints.
package endpoint

import "net/url"

// Endpoint - Informations about endpoint.
type Endpoint struct {
	ID        string   `json:"id,omitempty"`
	Transport string   `json:"transport,omitempty"`
	Addr      string   `json:"address,omitempty"`
	Health    bool     `json:"health,omitempty"`
	Auth      []string `json:"-"`
	// health monitoring
	watching bool      `json:"-"`
	watch    chan bool `json:"-"`
}

// New - Creates a new endpoint.
func New(address string, auth ...string) *Endpoint {
	uri, _ := url.Parse(address)
	return &Endpoint{
		Addr:      uri.Host,
		Transport: uri.Scheme,
		Health:    true,
		Auth:      auth,
	}
}

// UpdateHealth - Updates endpoint health and if one is watching it will get value on a channel.
func (endpoint *Endpoint) UpdateHealth(health bool) {
	endpoint.Health = health
	if endpoint.watching {
		endpoint.watch <- health
	}
}

// Dead - Updates endpoint health to false.
func (endpoint *Endpoint) Dead() {
	endpoint.UpdateHealth(false)
}

// Watch - Creates a channel that one will get health changes from.
func (endpoint *Endpoint) Watch() chan bool {
	endpoint.watching = true
	if endpoint.watch == nil {
		endpoint.watch = make(chan bool)
	}
	return endpoint.watch
}

// Stop - Stops receiving health changes on a channel.
func (endpoint *Endpoint) Stop() {
	endpoint.watching = false
	close(endpoint.watch)
}

// URL - Returns url constructed {endpoint.Transport}://{endpoint.Addr}.
func (endpoint *Endpoint) URL() string {
	if endpoint.Transport == "" {
		return endpoint.Addr
	}
	return endpoint.Transport + "://" + endpoint.Addr
}
