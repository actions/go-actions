package token

import . "testing"
import . "github.com/smartystreets/goconvey/convey"

func TestNew(t *T) {
	Convey("New", t, func() {
		Convey("It should create a new token", func() {
			id := New("username", "password")
			So(id.ClientID, ShouldEqual, "username")
			So(id.Token, ShouldEqual, "password")
		})
	})
}
