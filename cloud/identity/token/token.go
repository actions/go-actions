package token

import "time"
import "github.com/crackcomm/go-actions/cloud/access"

// Token - Access token.
type Token struct {
	ID       string            `json:"id"`
	ClientID string            `json:"client"`
	Token    string            `json:"token"`
	Created  time.Time         `json:"created"`
	Access   *access.AccessMap `json:"access"`
}

// New - Creates a new token from ClientID and Token.
func New(client, token string) *Token {
	return &Token{ClientID: client, Token: token}
}
