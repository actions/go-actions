package identity

import . "testing"
import . "github.com/smartystreets/goconvey/convey"

func TestUserIdentity(t *T) {
	Convey("UserIdentity", t, func() {
		Convey("It should create identity with username and password", func() {
			id := UserIdentity("username", "password")
			So(id.Username, ShouldEqual, "username")
			So(id.Password, ShouldEqual, "password")
		})
	})
}

func TestTokenIdentity(t *T) {
	Convey("TokenIdentity", t, func() {
		Convey("It should create identity with clientID and token", func() {
			id := TokenIdentity("clientID", "token")
			So(id.Token.ClientID, ShouldEqual, "clientID")
			So(id.Token.Token, ShouldEqual, "token")
		})
	})
}
