// Package identity provides abstracts for dealing with identity and authorization in the cloud.
package identity

import "github.com/crackcomm/go-actions/cloud/identity/token"
import "github.com/crackcomm/go-actions/cloud/endpoint"

// Identity - Cloud user identity used to authorize API calls. Only ClientID + Token or Username + Password can be used to authorize.
type Identity struct {
	Endpoints []*endpoint.Endpoint `json:"endpoints"` // your endpoints
	Username  string               `json:"username"`
	Password  string               `json:"password"`
	Token     *token.Token         `json:"token"`
}

// UserIdentity - Creates new Identity using Username and Password.
func UserIdentity(username, password string) *Identity {
	return &Identity{Username: username, Password: password}
}

// TokenIdentity - Creates new Identity using ClientID and Token.
func TokenIdentity(client, t string) *Identity {
	return &Identity{Token: token.New(client, t)}
}
