// Package worker defines utilities to manage workers.
package worker

import "errors"
import "github.com/crackcomm/go-actions/action"
import "github.com/crackcomm/go-actions/cloud/endpoint"
import "github.com/crackcomm/go-actions/core/functions"

// ErrNoRunner - Error returned when Runner was not set and tried to run.
var ErrNoRunner = errors.New("Worker runner is not defined")

// Current - Current worker, used for shortcuts.
var Current = LocalWorker()

// Close - Closes all current worker servers and sets all endpoints health to false.
func Close() {
	Current.Close()
}

// Action - Adds action to a current worker core.
func Action(name string, a *action.Action) {
	Current.Action(name, a)
}

// Alert - Adds alert to the current worker alerts list.
func Alert(event, message string) {
	Current.Alert(event, message)
}

// Breal - Adds alert to the current worker alerts list and informs worker needs a break (in seconds).
func Break(event, message string, breaksec int64) {
	Current.Break(event, message, breaksec)
}

// Join - Joins to a cluster through a storage like etcd.
// func Join(storage, collection string, machines ...string) error {
// 	return Current.Join(storage, collection, machines...)
// }

// Handler - Adds function to a current worker core.
func Handler(name string, function functions.Function) {
	Current.Handler(name, function)
}

// Endpoint - Adds a worker endpoint.
func Endpoint(address string, auth ...string) {
	Current.Endpoint(address, auth...)
}

// Endpoints - Returns current worker endpoints.
func Endpoints() []*endpoint.Endpoint {
	return Current.Endpoints
}

// Source - Adds actions sources to a worker core.
func Source(address string, authorization ...string) error {
	return Current.Source(address, authorization...)
}

// EndpointServer - Starts a servers from endpoint, does not add endpoint to the list.
func EndpointServer(e *endpoint.Endpoint) (err error) {
	return Current.EndpointServer(e)
}

// Server - Starts a servers from endpoint.
func Server(address string, auth ...string) error {
	return Current.Server(address, auth...)
}

// Start - Starts to listening on all endpoints until receives a signal to gracefully close
func Start() error {
	return Current.Start()
}

// Run - Runs action on a current worker.
func Run(a *action.Action) (action.Map, error) {
	return Current.Run(a)
}

// Call - Calls action on a current worker.
func Call(name string, ctx ...action.Map) (action.Map, error) {
	return Current.Call(name, ctx...)
}
