// Package alert defines alert structure and alerts monitoring structure.
package alert

import "time"

// Alert - Alert is an information send by worker when some trigger was raised.
type Alert struct {
	Message string    `json:"message,omitempty"` // alert message (eq. "CPU is 90%")
	Event   string    `json:"event,omitempty"`   // eq. "error"
	Break   int64     `json:"break,omitempty"`   // break in seconds that worker needs after this alert
	Created time.Time `json:"created,omitempty"`
}

// Alerts - Alerts list.
type Alerts struct {
	List     []*Alert    `json:"list,omitempty"`
	channel  chan *Alert `json:"-"`
	watching bool        `json:"-"`
}

// New - Creates a new alert.
func New(event, message string) *Alert {
	return &Alert{
		Created: time.Now(),
		Message: message,
		Event:   event,
	}
}

// Break - Creates a new alert. breaksec is a paramete informing a worker needs a break (in seconds).
func Break(event, message string, breaksec int64) (alert *Alert) {
	alert = New(event, message)
	alert.Break = breaksec
	return
}

// NewAlerts - Creates a new alerts list.
func NewAlerts() *Alerts {
	return &Alerts{
		List: []*Alert{},
	}
}

// All - Returns all alerts on the list.
func (a *Alerts) All() []*Alert {
	return a.List
}

// Add - Adds alert to a list. If one is watching it also pushes it to alerts channel.
func (a *Alerts) Add(alert *Alert) {
	if a.watching {
		a.channel <- alert
	}
	a.List = append(a.List, alert)
}

// Watch - Starts watching on alerts. Returns alerts.channel. When alert will be added it will be pushed to it.
// When one is stoping to receive on alerts channel it should be closed with alerts.Stop().
func (a *Alerts) Watch() chan *Alert {
	a.watching = true
	if a.channel == nil {
		a.channel = make(chan *Alert)
	}
	return a.channel
}

// Stop - Stops watching on alerts channel. It closes alerts channel.
func (a *Alerts) Stop() {
	a.watching = false
	close(a.channel)
}
