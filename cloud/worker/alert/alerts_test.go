package alert

import . "testing"
import . "github.com/smartystreets/goconvey/convey"

func TestAlert(t *T) {
	Convey("Alert", t, func() {
		Convey("New()", func() {
			Convey("It should create new alert", func() {
				alert := New("panic", "Function paniced")
				So(alert.Created, ShouldNotEqual, nil)
				So(alert.Message, ShouldEqual, "Function paniced")
				So(alert.Event, ShouldEqual, "panic")
				So(alert.Break, ShouldEqual, 0)
			})
		})
		Convey("Break()", func() {
			Convey("It should create new alert with break parameter", func() {
				alert := Break("panic", "Function paniced", 10)
				So(alert.Created, ShouldNotEqual, nil)
				So(alert.Message, ShouldEqual, "Function paniced")
				So(alert.Event, ShouldEqual, "panic")
				So(alert.Break, ShouldEqual, 10)
			})
		})
	})
}

func TestAlerts(t *T) {
	Convey("Alerts", t, func() {
		Convey("NewAlerts()", func() {
			Convey("It should create new alerts", func() {
				alerts := NewAlerts()
				So(alerts.List, ShouldNotEqual, nil)
			})
		})
		Convey("Alerts.All()", func() {
			Convey("It should return all alerts", func() {
				alerts := NewAlerts()
				alerts.Add(&Alert{})
				So(len(alerts.List), ShouldEqual, len(alerts.All()))
			})
		})
		Convey("Alerts.Watch()", func() {
			Convey("It should watch on alerts", func() {
				sent := New("info", "test")
				alerts := NewAlerts()
				Convey("It should receive alert", func() {
					go func() {
						for {
							if alerts.watching {
								alerts.Add(sent)
								break
							}
						}
					}()
					alert := <-alerts.Watch()
					So(alert, ShouldEqual, sent)
					So(len(alerts.List), ShouldEqual, 1)
					alerts.Stop()
				})
			})
		})
	})
}
