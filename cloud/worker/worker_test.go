package worker

import "errors"
import . "testing"
import . "github.com/smartystreets/goconvey/convey"
import "github.com/crackcomm/go-actions/action"
import filesource "github.com/crackcomm/go-actions/source/file"
import httpsource "github.com/crackcomm/go-actions/source/http"
import "github.com/crackcomm/go-actions/runner"
import "github.com/crackcomm/go-actions/cloud/endpoint"
import "github.com/crackcomm/go-actions/transport/server"

func testfunc(_ action.Map) (action.Map, error) { return action.Map{"success": true}, nil }

var testerr = errors.New("test server error")

func TestWorker(t *T) {
	Convey("Worker", t, func() {
		Convey("Worker.Handler()", func() {
			Convey("It should add core function", func() {
				a := &action.Action{}
				Action("test.action", a)
				g, _ := Current.Core.Sources.Get("test.action")
				So(g, ShouldEqual, a)
			})
		})
		Convey("Worker.Handler()", func() {
			Convey("It should add core function", func() {
				Handler("test.function", testfunc)
				So(Current.Core.Functions["test.function"], ShouldEqual, testfunc)
			})
		})
		Convey("Worker.Endpoints()", func() {
			Convey("It should return worker endpoints", func() {
				endpoints := Endpoints()
				So(len(endpoints), ShouldEqual, len(Current.Endpoints))
			})
		})
		Convey("Worker.Endpoint()", func() {
			Convey("It should add worker endpoint", func() {
				now := len(Current.Endpoints)
				Endpoint("test://test")
				So(now+1, ShouldEqual, len(Current.Endpoints))
				Convey("Worker.Endpoint()", func() {
					err := Start()
					So(err, ShouldEqual, testerr)
				})
			})
		})
		Convey("Worker.Source()", func() {
			Convey("It should add sources", func() {
				err := Source("file://filesource1")
				So(err, ShouldEqual, nil)
				err = Source("http://source2.com/")
				So(err, ShouldEqual, nil)
				sources := Current.Core.Sources.List
				So(len(sources), ShouldEqual, 3)
				// sources[0] is action.Actions
				src1, ok := sources[1].(*filesource.Source)
				ShouldBeTrue(ok)
				So(src1.Addr, ShouldEqual, "filesource1")
				src2, ok := sources[2].(*httpsource.Source)
				ShouldBeTrue(ok)
				So(src2.Addr, ShouldEqual, "http://source2.com/")
			})
			Convey("It report error when source was not created", func() {
				err := Source("nononono://filesource1")
				So(err.Error(), ShouldEqual, `Action source constructor for transport "nononono" was not found`)
			})
		})
		Convey("Worker.Run()", func() {
			Convey("It should run action on local runner", func() {
				Handler("fail.function", testfunc)
				res, err := Run(&action.Action{Name: "fail.function"})
				So(err, ShouldEqual, nil)
				success, ok := res.Get("success").Bool()
				ShouldBeTrue(ok)
				So(success, ShouldEqual, true)
			})
			Convey("It should run error when no runner is set", func() {
				worker := &Worker{}
				_, err := worker.Run(&action.Action{})
				So(err, ShouldEqual, ErrNoRunner)
			})
		})
		Convey("Worker.Call()", func() {
			Convey("It should call action on local runner", func() {
				Handler("fail.function", testfunc)
				res, err := Call("fail.function")
				So(err, ShouldEqual, nil)
				success, ok := res.Get("success").Bool()
				ShouldBeTrue(ok)
				So(success, ShouldEqual, true)
			})
			Convey("It should call error when no runner is set", func() {
				worker := &Worker{}
				_, err := worker.Call("fail.function")
				So(err, ShouldEqual, ErrNoRunner)
			})
		})
		Convey("Worker.Server()", func() {
			w := LocalWorker()
			Convey("It should start a server", func() {
				err := w.Server("test://127.0.0.1:8080")
				So(err, ShouldEqual, testerr)
			})
			Convey("It should report error when server was not created", func() {
				err := w.Server("nononono://127.0.0.1:8080")
				So(err.Error(), ShouldEqual, `Server constructor for transport "nononono" was not found`)
			})
		})
		Convey("Server()", func() {
			Convey("It should start a server", func() {
				err := Server("test://127.0.0.1:8080")
				So(err, ShouldEqual, testerr)
			})
		})
		Convey("Worker.EndpointServer()", func() {
			Convey("It should start a server from endpoint", func() {
				e := endpoint.New("test://fsdsdfsd")
				err := EndpointServer(e)
				So(err, ShouldEqual, testerr)
			})
		})
		Convey("Worker.Start()", func() {
			Convey("It should return nil when no endpoints were set", func() {
				w := LocalWorker()
				err := w.Start()
				So(err, ShouldEqual, nil)
			})
			Convey("It should report error when endpoint server was not constructed", func() {
				w := LocalWorker()
				w.Endpoint("notfound://test")
				err := w.Start()
				So(err.Error(), ShouldEqual, `Server constructor for transport "notfound" was not found`)
			})
		})
		Convey("Worker.Close()", func() {
			Convey("It should close all servers", func() {
				w := LocalWorker()
				w.Endpoints = []*endpoint.Endpoint{{Health: true}, {Health: true}}
				w.Servers = []server.Server{new(testServer), new(testServer)}
				w.Close()
				for _, srv := range w.Servers {
					s, _ := srv.(*testServer)
					So(s.closed, ShouldEqual, true)
				}
				for _, point := range w.Endpoints {
					So(point.Health, ShouldEqual, false)
				}
			})
		})
		Convey("Close()", func() {
			Convey("It should close all servers", func() {
				Current.Servers = []server.Server{new(testServer), new(testServer)}
				Close()
				for _, srv := range Current.Servers {
					s, _ := srv.(*testServer)
					So(s.closed, ShouldEqual, true)
				}
			})
		})
		Convey("Alerts", func() {
			Convey("Alerts.Watch()", func() {
				Convey("It should get alert", func() {
					go func() {
						Convey("Alert() and Break()", t, func() {
							Convey("It should add alert", func() {
								Alert("test", "message")
								Break("test2", "message2", 60)
							})
						})
					}()
					a := <-Current.Alerts.Watch()
					So(a.Message, ShouldEqual, "message")
					So(a.Event, ShouldEqual, "test")
					So(a.Break, ShouldEqual, 0)
					b := <-Current.Alerts.Watch()
					So(b.Message, ShouldEqual, "message2")
					So(b.Event, ShouldEqual, "test2")
					So(b.Break, ShouldEqual, 60)
				})
			})
		})
	})
}

type testServer struct {
	closed bool
}

func (server *testServer) Start() error {
	return testerr
}

func (server *testServer) Close() error {
	server.closed = true
	return nil
}

func init() {
	server.Register("test", func(_ string, _ runner.Runner, _ ...string) (server.Server, error) {
		return new(testServer), nil
	})
}
