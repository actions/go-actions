// Package worker defines utilities to manage workers.
package worker

import "github.com/golang/glog"
import "github.com/crackcomm/go-actions/core"
import "github.com/crackcomm/go-actions/local"
import "github.com/crackcomm/go-actions/runner"
import "github.com/crackcomm/go-actions/action"
import "github.com/crackcomm/go-actions/source"
import "github.com/crackcomm/go-actions/core/functions"
import "github.com/crackcomm/go-actions/cloud/endpoint"
import "github.com/crackcomm/go-actions/transport/server"
import "github.com/crackcomm/go-actions/cloud/worker/alert"
import "github.com/crackcomm/go-actions/cloud/identity/token"

// Worker - Describes basic properties about a worker and helps to create one.
type Worker struct {
	ID        string               `json:"id"`
	Health    bool                 `json:"health,omitempty"`
	Functions []string             `json:"functions,omitempty"`
	Endpoints []*endpoint.Endpoint `json:"endpoints,omitempty"`
	Alerts    *alert.Alerts        `json:"alerts,omitempty"`
	Servers   []server.Server      `json:"-"`
	Runner    runner.Runner        `json:"-"`
	Token     *token.Token         `json:"-"`
	Core      *core.Core           `json:"-"`
	// Cluster   *Cluster             `json:"cluster,omitempty"`
}

// LocalWorker - Creates a new local worker using clone of a default core and a local runner.
func LocalWorker() *Worker {
	w := New()
	w.Core = core.Clone()
	w.Runner = &local.Runner{Core: w.Core}
	return w
}

// New - Creates a new empty w.
func New() *Worker {
	return &Worker{
		Endpoints: []*endpoint.Endpoint{},
		Functions: []string{},
		Servers:   []server.Server{},
		Alerts:    alert.NewAlerts(),
	}
}

// Action - Adds action to a worker core.
func (w *Worker) Action(name string, a *action.Action) {
	w.Core.Action(name, a)
}

// Alert - Adds alert to the list.
func (w *Worker) Alert(event, message string) {
	a := alert.New(event, message)
	w.Alerts.Add(a)
}

// Breal - Adds alert to the list and informs worker needs a break (in seconds).
func (w *Worker) Break(event, message string, breaksec int64) {
	a := alert.Break(event, message, breaksec)
	w.Alerts.Add(a)
}

// Handler - Adds function to a worker core.
func (w *Worker) Handler(name string, function functions.Function) {
	w.Core.Handler(name, function)
}

// Source - Adds actions sources to a worker core.
func (w *Worker) Source(address string, authorization ...string) (err error) {
	src, err := source.Create(address, authorization...)
	if err != nil {
		return
	}
	w.Core.Sources.More(src)
	return nil
}

// Endpoint - Adds a worker endpoint.
func (w *Worker) Endpoint(address string, auth ...string) {
	glog.Infof("New worker endpoint address=%s", address)
	w.Endpoints = append(w.Endpoints, endpoint.New(address, auth...))
}

// Start - Starts to listening on all endpoints until receives a signal to gracefully close
func (w *Worker) Start() (err error) {
	glog.Infoln("Starting a worker")

	if len(w.Endpoints) == 0 {
		glog.Warning("No endpoints")
		return nil
	}

	// Create error channel
	errchan := make(chan error)

	// Start servers on all endpoints
	for _, e := range w.Endpoints {
		go func(e *endpoint.Endpoint) {
			err := w.EndpointServer(e)
			if err != nil {
				errchan <- err
			}
		}(e)
	}

	// TODO: os.Signals
	err = <-errchan
	if err != nil {

	}
	w.Close()
	return
}

// EndpointServer - Starts a servers from endpoint, does not add endpoint to the list.
func (w *Worker) EndpointServer(e *endpoint.Endpoint) (err error) {
	err = w.Server(e.URL(), e.Auth...)
	if err != nil {
		e.Dead()
	}
	return
}

// Server - Starts a servers. (does not add endpoint to the list)
func (w *Worker) Server(address string, auth ...string) (err error) {
	// Get server by transport
	srv, err := server.Create(address, w, auth...)
	if err != nil {
		glog.Warningf("Server construction failed address=%s err=%v", address, err)
		return
	}

	// Add a server to the list
	w.Servers = append(w.Servers, srv)

	// Start server
	glog.Infof("Starting a server (address=%s)", address)
	err = srv.Start()
	if err != nil {
		glog.Infof("Server error %v (address=%s)", err, address)
		srv.Close()
	}

	return err
}

// Call - Calls action on a worker runner.
func (w *Worker) Call(name string, ctx ...action.Map) (action.Map, error) {
	if w.Runner == nil {
		return nil, ErrNoRunner
	}
	return w.Runner.Call(name, ctx...)
}

// Run - Runs action on a worker runner.
func (w *Worker) Run(a *action.Action) (action.Map, error) {
	if w.Runner == nil {
		return nil, ErrNoRunner
	}
	return w.Runner.Run(a)
}

// Join - Joins to a cluster through a storage like etcd.
// func (w *Worker) Join(storage, collection string, machines ...string) error {
// 	cluster, err := NewCluster(storage, collection, machines...)
// 	if err != nil {
// 		return err
// 	}
// 	w.Cluster = cluster
// 	return nil
// }

// Close - Closes all worker servers and sets all endpoints health to false.
func (w *Worker) Close() {
	// Set all endpoints to dead
	for _, point := range w.Endpoints {
		point.Dead()
	}

	// Close all servers
	for _, srv := range w.Servers {
		srv.Close()
	}
}
