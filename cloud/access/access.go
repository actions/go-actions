// Package access defines structures for describing access scope granted by tokens.
package access

// AccessMap - Describes access scope.
type AccessMap struct {
	Full      bool      `json:"full,omitempty"`      // if true full access is granted
	Actions   []*Access `json:"actions,omitempty"`   // read/write/run access to actions
	Functions []*Access `json:"functions,omitempty"` // access to call a function
	Workers   []*Access `json:"workers,omitempty"`   // read/write/run access to workers
	Clusters  []*Access `json:"clusters,omitempty"`  // read/write/run access on cluster
}

// Access - Describes access to entity. For example a function, worker, cluster or a action.
type Access struct {
	ID     string `json:"id"`               // entity id
	Delete bool   `json:"delete,omitempty"` // delete access
	Write  bool   `json:"write,omitempty"`  // write access
	Read   bool   `json:"read,omitempty"`   // read access
	Run    bool   `json:"run,omitempty"`    // run access
}
