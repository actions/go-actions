package storage

import "errors"
import . "testing"
import . "github.com/smartystreets/goconvey/convey"

func TestRegister(t *T) {
	Convey("Register", t, func() {
		Convey("It should register storage constructor", func() {
			tc := new(testConstructor)
			Register("new", tc)
			So(Default["new"], ShouldEqual, tc)
		})
	})
}

func TestCreate(t *T) {
	Convey("Create", t, func() {
		Convey("It should create storage", func() {
		})
		Convey("It should return error", func() {
			_, err := Create("test", "error")
			So(err, ShouldEqual, testerr)
		})
		Convey("It should return error when constructor was not found", func() {
			_, err := Create("non", "error")
			So(err.Error(), ShouldEqual, `Constructor "non" was not found`)
		})
	})
}

var testerr = errors.New("test error")

type testConstructor int

func (c *testConstructor) Create(collection string, machines ...string) (Storage, error) {
	if collection == "error" {
		return nil, testerr
	}
	return nil, nil
}

func init() {
	Register("test", new(testConstructor))
}
