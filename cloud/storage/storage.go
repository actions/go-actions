// Package storage defines interface for creating Key-Value storage clients.
package storage

import "fmt"

// Storage - Cluster store interface.
type Storage interface {
	All(key string) (map[string][]byte, error)
	Get(key string) ([]byte, error)
	Set(key string, value []byte, ttl uint64) error
	Delete(key string) error
}

// Constructors - Holds many storage constructors.
type Constructors map[string]Constructor

// Constructor - Function initating storage constructor.
type Constructor interface {
	Create(collection string, machines ...string) (Storage, error)
}

// Default - Default constructors.
var Default = make(Constructors)

// Register - Registers a new constructor.
func (b Constructors) Register(name string, constructor Constructor) {
	b[name] = constructor
}

// Create - Create a storage from constructor name.
func (b Constructors) Create(name, collection string, machines ...string) (Storage, error) {
	constructor := b[name]
	if constructor == nil {
		return nil, fmt.Errorf("Constructor %#v was not found", name)
	}
	return constructor.Create(collection, machines...)
}

// Register - Registers a new constructor to default map.
func Register(name string, constructor Constructor) {
	Default.Register(name, constructor)
}

// Create - Creates a storage from default constructors.
func Create(name, collection string, machines ...string) (Storage, error) {
	return Default.Create(name, collection, machines...)
}
