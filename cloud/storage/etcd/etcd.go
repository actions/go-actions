// Package etcd defines Key-Value etcd storage client.
package etcd

import "path"
import "strings"
import "github.com/coreos/go-etcd/etcd"
import "github.com/crackcomm/go-actions/cloud/storage"

// Storage - Implements etcd Key-Value storage client.
type Storage struct {
	Collection string
	client     *etcd.Client
}

// New - Creates a new etcd storage.
func New(collection string, machines ...string) *Storage {
	return &Storage{
		Collection: collection,
		client:     etcd.NewClient(machines),
	}
}

// All - Gets all values from storage under a key.
func (s *Storage) All(key string) (bodies map[string][]byte, err error) {
	key = path.Join(s.Collection, key)
	response, err := s.client.Get(key, false, true)
	if err != nil {
		return
	}
	bodies = make(map[string][]byte)
	for _, node := range response.Node.Nodes {
		nodekey := strings.Trim(node.Key[len(s.Collection)+1:], "/")
		bodies[nodekey] = []byte(node.Value)
	}
	return
}

// Get - Gets a value from storage by key.
func (s *Storage) Get(key string) (body []byte, err error) {
	key = path.Join(s.Collection, key)
	response, err := s.client.Get(key, false, false)
	if err != nil {
		return
	}
	body = []byte(response.Node.Value)
	return
}

// Set - Sets a value under a key in a storage.
func (s *Storage) Set(key string, value []byte, ttl uint64) (err error) {
	key = path.Join(s.Collection, key)
	_, err = s.client.Set(key, string(value), ttl)
	return
}

// Delete - Deletes a value from a storage by key.
func (s *Storage) Delete(key string) (err error) {
	key = path.Join(s.Collection, key)
	_, err = s.client.Delete(key, true)
	return
}

// Machines - Sets etcd machines.
func (s *Storage) Machines(machines ...string) {
	s.client.SetCluster(machines)
}

// constructor - etcd storage constructor
type constructor int

// Create - Creates a new etcd storage.
func (_ *constructor) Create(collection string, machines ...string) (storage.Storage, error) {
	return New(collection, machines...), nil
}

func init() {
	storage.Register("etcd", new(constructor))
}
