// Package runner defines action runner interface.
package runner

import "fmt"
import "math/rand"
import "github.com/crackcomm/go-actions/action"

// Runner - Runner Interface.
type Runner interface {
	Run(a *action.Action) (action.Map, error)
	Call(name string, ctx ...action.Map) (action.Map, error) // context's have to be merged -- last is merged into previous etc.
}

// Runners - List of runners.
type Runners []Runner

// Run - Runs action on a random runner.
func (runners Runners) Run(a *action.Action) (action.Map, error) {
	runner := runners.Random()
	return runner.Run(a)
}

// Call - Calls action on a random runner.
func (runners Runners) Call(name string, ctx ...action.Map) (action.Map, error) {
	runner := runners.Random()
	return runner.Call(name, ctx...)
}

// Random - Returns a random runner from a list.
func (runners Runners) Random() Runner {
	if len(runners) == 0 {
		return nil
	} else if len(runners) > 1 {
		max := len(runners)
		fmt.Printf("max %#v\n", max)
		num := rand.Intn(max)
		fmt.Printf("num %#v\n", num)
		return runners[num]
	}
	return runners[0]
}
