// Package group defines groups for action execution.
package group

import "sync"
import "github.com/crackcomm/go-actions/local"
import "github.com/crackcomm/go-actions/action"
import "github.com/crackcomm/go-actions/runner"

// Group - Group for actions execution.
type Group struct {
	Runners runner.Runners
	Results []action.Map
	waiting bool
	done    bool
	end     chan error
	lock    *sync.RWMutex
	running int64
}

// New - Creates a new running group.
func New(runners ...runner.Runner) *Group {
	if len(runners) == 0 {
		runners = append(runners, local.DefaultRunner)
	}
	return &Group{
		Runners: runners,
		lock:    new(sync.RWMutex),
		end:     make(chan error, 1),
	}
}

// Call - Adds action to execute by name.
func (g *Group) Call(name string, contexts ...action.Map) bool {
	return g.Add(action.New(name, contexts...))
}

// Add - Adds action to execute.
func (g *Group) Add(a *action.Action) bool {
	// If done -- no more actions
	if g.done {
		return false
	}

	// Add one running action
	g.lock.Lock()
	g.running += 1
	g.lock.Unlock()

	go func() {
		// Run action on a random selected runner
		res, err := g.Runners.Run(a)

		// Add result to Results list
		g.lock.Lock()
		if err == nil {
			g.Results = append(g.Results, res)
		}
		g.running -= 1
		g.lock.Unlock()

		// If error occured or waiting and no more running -- set done
		if err != nil || (g.waiting && g.running <= 0) {
			g.setdone(err)
		}
	}()
	return true
}

// Wait - Returns channel that will return when all actions will be completed.
func (g *Group) Wait() chan error {
	g.waiting = true
	if g.done || g.running <= 0 {
		g.setdone(nil)
	}
	return g.end
}

// setdone - Sets group as done, no more actions can be runned.
func (g *Group) setdone(err error) {
	g.done = true
	g.end <- err
}
