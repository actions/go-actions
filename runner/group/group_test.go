package group

import "time"
import "errors"
import . "testing"
import . "github.com/crackcomm/go-actions/action"
import . "github.com/smartystreets/goconvey/convey"
import "github.com/crackcomm/go-actions/core"
import "github.com/crackcomm/go-actions/local"

var ErrTest = errors.New("Timeout")
var ErrTestDelay = errors.New("Timeout delayed -- WRONG!")

func delayError(_ Map) (Map, error) {
	<-time.After(20 * time.Millisecond)
	return nil, ErrTestDelay
}

func nowError(_ Map) (Map, error) {
	return nil, ErrTest
}

func noop(_ Map) (Map, error) {
	return Map{}, nil
}

func TestGroup(t *T) {
	r := &local.Runner{Core: core.New()}
	r.Core.Add("noop", &core.Function{Function: noop})
	r.Core.Add("error.now", &core.Function{Function: nowError})
	r.Core.Add("error.delay", &core.Function{Function: delayError})
	Convey("Group", t, func() {
		Convey("It should contain local runner by default", func() {
			g := New()
			So(len(g.Runners), ShouldEqual, 1)
			So(g.Runners[0], ShouldEqual, local.DefaultRunner)
		})
		Convey("It should finish two actions before returning", func() {
			g := New(r)
			ShouldBeTrue(g.Add(&Action{Name: "noop"}))
			ShouldBeTrue(g.Add(&Action{Name: "noop"}))
			err := <-g.Wait()
			So(err, ShouldEqual, nil)
			So(len(g.Results), ShouldEqual, 2)
		})
		Convey("It should report error after first action", func() {
			g := New(r)
			ShouldBeTrue(g.Add(&Action{Name: "error.delay"}))
			ShouldBeTrue(g.Add(&Action{Name: "error.now"}))
			err := <-g.Wait()
			So(err, ShouldEqual, ErrTest)
		})
		Convey("It should not wait when no actions were added", func() {
			g := New()
			err := <-g.Wait()
			So(err, ShouldEqual, nil)
			Convey("Add() should return false when already done", func() {
				ok := g.Add(&Action{Name: "noop"})
				So(ok, ShouldEqual, false)
			})
		})
	})
}
