package core

import "errors"
import . "testing"
import "github.com/crackcomm/go-actions/action"
import . "github.com/smartystreets/goconvey/convey"
import _ "github.com/crackcomm/go-actions/source/mem"

var ErrTest = errors.New("Test error")

func BenchmarkResolve(b *B) {
	Action("http.request3", &action.Action{
		Name: "http.request2",
		Ctx:  action.Map{"method": "GET"},
		Next: &action.Action{
			Name: "html.extract",
		},
	})
	Action("http.request2", &action.Action{
		Name: "http.request",
		Ctx:  action.Map{"method": "GET"},
		Next: &action.Action{
			Name: "html.extract",
		},
	})
	Action("http.get", &action.Action{
		Name: "http.request3",
		Ctx:  action.Map{"method": "GET"},
	})
	for i := 0; i < b.N; i++ {
		Resolve(&action.Action{Name: "http.get"})
	}
}

func TestAction(t *T) {
	Convey("Core.Action()", t, func() {
		Convey("It should add action", func() {
			a := &action.Action{Name: "bu"}
			Action("test.action", a)
			b, _ := Default.Sources.Get("test.action")
			So(a, ShouldEqual, b)
		})
		Convey("It should add action", func() {
			c := New()
			a := &action.Action{Name: "bu"}
			c.Action("test.action", a)
			b, _ := c.Sources.Get("test.action")
			So(a, ShouldEqual, b)
		})
	})
}

func TestClone(t *T) {
	Convey("Core.Clone()", t, func() {
		Convey("It should clone core", func() {
			clone := Clone()
			So(len(clone.Functions), ShouldEqual, len(Default.Functions))
			So(clone.Sources.Len(), ShouldEqual, Default.Sources.Len())
		})
	})
}

func TestGet(t *T) {
	Convey("core.Handler() and core.Get()", t, func() {
		Convey("It should get function", func() {
			fnc := func(m action.Map) (action.Map, error) {
				return m, nil
			}
			Handler("test", fnc)
			So(Get("test"), ShouldEqual, fnc)
		})
	})
}

func TestHas(t *T) {
	Convey("Core.Has()", t, func() {
		fnc := func(m action.Map) (action.Map, error) { return m, nil }
		Handler("test", fnc)
		Convey("It should return true", func() {
			So(Has("test"), ShouldEqual, true)
		})
		Convey("It should return false", func() {
			So(Has("nonexistent"), ShouldEqual, false)
		})
	})
}

// func TestCall(t *T) {
// 	Convey("Core.Call()", t, func() {
// 		errfunc := func(m action.Map) (action.Map, error) { return nil, ErrTest }
// 		test := func(m action.Map) (action.Map, error) { return m, nil }
// 		Add("errfunc", &Function{Function: errfunc})
// 		Add("test", &Function{Function: test})
// 		Convey("It should call function and return value", func() {
// 			context := action.Map{"some": "test"}
// 			res, err := Call("test", context)
// 			So(err, ShouldEqual, nil)
// 			So(res, ShouldEqual, context)
// 		})
// 		Convey("It should call function and return error", func() {
// 			res, err := Call("errfunc", nil)
// 			So(res, ShouldEqual, nil)
// 			So(err, ShouldEqual, ErrTest)
// 		})
// 	})
// }

func TestRegister(t *T) {
	Convey("Core.Register()", t, func() {
		fnc := func(m action.Map) (action.Map, error) { return m, nil }
		o := New()
		o.Handler("test", fnc)
		Convey("It should register core under prefix", func() {
			Register("some", o)
			So(Get("some.test"), ShouldEqual, fnc)
		})
	})
}

func TestResolve(t *T) {
	Convey("Core.Resolve()", t, func() {
		fnc := func(m action.Map) (action.Map, error) { return m, nil }
		Handler("html.extract", fnc)
		Handler("http.request", fnc)
		Action("http.request3", &action.Action{
			Name: "http.request2",
			Ctx:  action.Map{"method": "GET"},
			Next: &action.Action{
				Name: "html.extract",
			},
		})
		Action("http.request2", &action.Action{
			Name: "http.request",
			Ctx:  action.Map{"method": "GET"},
			Next: &action.Action{
				Name: "html.extract",
			},
		})
		Action("http.get", &action.Action{
			Name: "http.request3",
			Ctx:  action.Map{"method": "GET"},
		})
		Convey("It should Resolve function", func() {
			a, err := Resolve(&action.Action{Name: "http.get"})
			So(err, ShouldEqual, nil)
			So(a.Name, ShouldEqual, "http.request")
			So(a.Ctx, ShouldNotEqual, nil)
			So(a.Next.Name, ShouldEqual, "html.extract")
			So(a.Next.Next.Name, ShouldEqual, "html.extract")
			method, ok := a.Ctx.Get("method").String()
			So(ok, ShouldEqual, true)
			So(method, ShouldEqual, "GET")
		})

		Convey("It should not resolve non existent action", func() {
			a, err := Resolve(&action.Action{
				Name: "html.extract",
				Next: &action.Action{
					Name: "other.non",
				},
			})
			So(a, ShouldEqual, nil)
			So(err, ShouldNotEqual, nil)
			So(err.Error(), ShouldEqual, "Action other.non was not found")
		})
	})
}

func TestSources(t *T) {
	Convey("Sources()", t, func() {
		Convey("It should add source to default core", func() {
			now := len(Default.Sources.List)
			Source("mem://test")
			So(len(Default.Sources.List), ShouldEqual, now+1)
		})
	})
}

func init() {
	Action("http.get", &action.Action{
		Name: "http.request",
		Ctx: action.Map{
			"method": "get",
		},
	})
}
