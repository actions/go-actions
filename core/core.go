// Package core defines structure holding functions that can be runned by action.
package core

import "errors"

import "github.com/golang/glog"
import "github.com/crackcomm/go-actions/action"
import "github.com/crackcomm/go-actions/source"
import "github.com/crackcomm/go-actions/core/functions"

// ErrActionEmpty - Error returned when tried to run nil action.
var ErrActionEmpty = errors.New("Action can not be empty.")

// Core - Holds core functions.
type Core struct {
	Sources   *source.Sources
	Functions functions.Functions
}

// New - Creates a new Core structure.
func New() *Core {
	return &Core{
		Functions: make(functions.Functions),
		Sources:   source.NewSources(action.NewActions()),
	}
}

// Action - Adds action to all sources.
func (core *Core) Action(name string, a *action.Action) error {
	return core.Sources.Add(name, a)
}

// Handle - Adds a function by name to the core.
func (core *Core) Handler(name string, fnc functions.Function) {
	core.Functions.Add(name, fnc)
}

// Has - Checks if function exists in core.
func (core *Core) Has(name string) bool {
	if core.Functions[name] != nil {
		return true
	}
	return false
}

// Call - Calls an action by name with flattened context.
func (core *Core) Call(name string, contexts ...action.Map) (action.Map, error) {
	return core.Run(&action.Action{Name: name, Ctx: action.Flat(contexts...)})
}

// Run - Runs an action.
func (core *Core) Run(a *action.Action) (action.Map, error) {
	// Resolve action to raw core function calls
	a, err := core.Resolve(a)
	if err != nil {
		return nil, err
	}
	return core.RawRun(a)
}

// RawRun - Runs a resolved action.
func (core *Core) RawRun(a *action.Action) (context action.Map, err error) {
	// Logging
	if glog.V(2) {
		glog.Infof("RUN %s %#v", a.Name, a.Ctx)
	} else if glog.V(1) {
		glog.Infof("RUN %s", a.Name)
	}

	// Run a core function
	context, err = core.Functions.Call(a.Name, a.Ctx)
	if err != nil {
		if glog.V(1) {
			glog.Infof("RUN ERROR %s %v", a.Name, err)
		}
		return
	}

	// Logging
	if glog.V(2) {
		glog.Infof("DONE %s %#v", a.Name, context)
	} else if glog.V(1) {
		glog.Infof("DONE %s", a.Name)
	}

	// Close if no next
	if a.Next == nil {
		return
	}

	// Merge results into next action context
	if !context.Empty() {
		a.Next.Ctx = context.Defaults(a.Next.Ctx)
	}

	// Run next action
	context, err = core.RawRun(a.Next)
	return
}

// Resolve - Resolve action.
func (core *Core) Resolve(a *action.Action) (*action.Action, error) {
	// Return error if action is nil
	if a == nil {
		return nil, ErrActionEmpty
	}

	// Reapeat if not core
	if core.Has(a.Name) {
		// If is a core call - resolve next action
		if a.Next != nil {
			var err error
			a.Next, err = core.Resolve(a.Next)
			if err != nil {
				return nil, err
			}
		}
		return a, nil
	}

	// Try to find action
	found, err := core.Sources.Get(a.Name)
	if err != nil {
		return nil, err
	}

	a.Name = found.Name

	// Set action context defaults
	if !found.Ctx.Empty() {
		if a.Ctx != nil {
			a.Ctx.Defaults(found.Ctx)
		} else {
			a.Ctx = found.Ctx.Clone()
		}
	}

	// Set or wrap next action if not empty
	if found.Next != nil {
		if a.Next != nil {
			a.Next = a.Next.Wrap(found.Next)
		} else {
			a.Next = found.Next.Clone()
		}
	}

	return core.Resolve(a)
}

// Source - Creates a source from address that should be a valid url like `file://actions` or `http://actions.com/`.
func (core *Core) Source(address string, authorization ...string) (err error) {
	src, err := source.Create(address, authorization...)
	if err != nil {
		glog.Infof("Source create error for address %#v: %v", address, err)
		return
	}
	glog.Infof("New source %s", address)
	core.Sources.More(src)
	return
}

// Clone - Clones core.
func (core *Core) Clone() *Core {
	return &Core{
		Functions: core.Functions.Clone(),
		Sources:   core.Sources.Clone(),
	}
}

// Register - Merges core functions into current core.
func (core *Core) Register(prefix string, c *Core) {
	for name, fnc := range c.Functions {
		// Join prefix to name if not empty
		if prefix != "" && name != "" {
			name = prefix + "." + name
		}
		core.Functions.Add(name, fnc)
	}
}
