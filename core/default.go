package core

import "github.com/crackcomm/go-actions/action"
import "github.com/crackcomm/go-actions/core/functions"

// Default - Default core.
var Default = New()

// Action - Adds action to all sources in default core.
func Action(name string, a *action.Action) error {
	return Default.Sources.Add(name, a)
}

// Handle - Adds function by name to the default core.
func Handler(name string, fnc functions.Function) {
	Default.Handler(name, fnc)
}

// Get - Get function from default core by name.
func Get(name string) functions.Function {
	return Default.Functions.Get(name)
}

// Has - Checks if function exists in default core.
func Has(name string) bool {
	return Default.Has(name)
}

// Run - Resolves and runs action using Default core.
func Run(a *action.Action) (action.Map, error) {
	return Default.Run(a)
}

// RawRun - Runs action using Default core without resolving.
func RawRun(a *action.Action) (action.Map, error) {
	return Default.Run(a)
}

// Call - Calls action using Default core.
func Call(name string, contexts ...action.Map) (action.Map, error) {
	return Default.Call(name, contexts...)
}

// Clone - Output a clone of a default core.
func Clone() *Core {
	return Default.Clone()
}

// Register - Adds functions to the default core.
func Register(prefix string, core *Core) {
	Default.Register(prefix, core)
}

// Resolve - Resolves actions to raw core actions using default core.
func Resolve(a *action.Action) (*action.Action, error) {
	return Default.Resolve(a)
}

// Source - Adds actions source to a default core.
func Source(address string, authorization ...string) error {
	return Default.Source(address, authorization...)
}
