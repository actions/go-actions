package functions

import . "testing"
import . "github.com/smartystreets/goconvey/convey"
import "github.com/crackcomm/go-actions/action"

func TestFunctions(t *T) {
	testFunc := func(context action.Map) (action.Map, error) {
		return action.Map{"success": true, "input": context}, nil
	}
	Convey("Functions", t, func() {
		fncs := make(Functions)
		fncs["test.func"] = testFunc

		Convey("Should be able to call a function", func() {
			input := action.Map{"ok": true}
			res, err := fncs.Call("test.func", input)

			// Check
			So(err, ShouldEqual, nil)
			So(res["success"], ShouldEqual, true)
			So(res["input"], ShouldEqual, input)
		})

		Convey("Should not call not existing function", func() {
			_, err := fncs.Call("not.found", nil)
			So(err.Error(), ShouldEqual, "Function not.found was not found")
		})
	})
	Convey("Functions.Clone()", t, func() {
		Convey("Should clone functions", func() {
			a := Functions{"a": testFunc}
			b := a.Clone()
			So(a, ShouldNotEqual, b)
			So(len(a), ShouldEqual, len(b))
			So(b["a"], ShouldEqual, testFunc)
		})
	})
	Convey("Functions.Add()", t, func() {
		Convey("Should add function", func() {
			a := Functions{}
			a.Add("a", testFunc)
			So(a["a"], ShouldEqual, testFunc)
		})
	})
	Convey("Functions.Get()", t, func() {
		Convey("Should get function by name", func() {
			a := Functions{}
			a.Add("a", testFunc)
			g := a.Get("a")
			So(g, ShouldEqual, testFunc)
		})
	})
}
