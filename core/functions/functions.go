package functions

import "fmt"
import "github.com/crackcomm/go-actions/action"

// Function - Core function interface.
type Function func(action.Map) (action.Map, error)

// Functions - Functions by name.
type Functions map[string]Function

// Add - Adds a function to a list.
func (f Functions) Add(name string, function Function) {
	f[name] = function
}

// Get - Gets a function by a name.
func (f Functions) Get(name string) Function {
	return f[name]
}

// Call - Calls a function by name
func (f Functions) Call(name string, context action.Map) (action.Map, error) {
	if fnc := f[name]; fnc != nil {
		return fnc(context)
	}
	return nil, fmt.Errorf("Function %s was not found", name)
}

// Clone - Clones functions.
func (f Functions) Clone() (c Functions) {
	c = make(Functions)
	for key, value := range f {
		c[key] = value
	}
	return
}
