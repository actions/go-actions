// Package source defines source interface.
package source

import "fmt"
import "errors"
import "github.com/crackcomm/go-actions/action"

// Sources - List of sources.
type Sources struct {
	List []Source
}

// ErrNotFound - Error returned when action was not found.
var ErrNotFound = errors.New("Action was not found")

// ErrEmptyName - Error returned when tried to find empty action name.
var ErrEmptyName = errors.New("Action name can not be empty")

// NewSources - Creates new Source list.
func NewSources(sources ...Source) *Sources {
	return &Sources{List: sources}
}

// All - Collect all actions from all sources.
func (sources *Sources) All() (actions action.Actions, err error) {
	actions = make(action.Actions)
	for _, src := range sources.List {
		var more action.Actions
		more, err = src.All()
		if err != nil {
			return
		}
		for name, a := range more {
			actions[name] = a
		}
	}
	return
}

// Add - Adds action to all sources on the list, returns first error that occurs.
func (sources *Sources) Add(name string, a *action.Action) (err error) {
	for _, src := range sources.List {
		err = src.Add(name, a)
		if err != nil {
			return
		}
	}
	return
}

// Get - Get action from all sources until its found.
func (sources *Sources) Get(name string) (*action.Action, error) {
	for _, src := range sources.List {
		action, err := src.Get(name)
		if err == nil && action != nil {
			return action, nil
		}
	}
	return nil, fmt.Errorf("Action %s was not found", name)
}

// More - More sources to the list.
func (sources *Sources) More(src ...Source) {
	sources.List = append(sources.List, src...)
}

// Clone - Clone sources.
func (sources *Sources) Clone() *Sources {
	return &Sources{List: sources.List}
}

// Len - Returns amount of sources on the list.
func (sources *Sources) Len() int {
	return len(sources.List)
}
