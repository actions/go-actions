package http

import "time"
import "net/http"
import . "testing"
import . "github.com/crackcomm/go-actions/action"
import . "github.com/smartystreets/goconvey/convey"
import "github.com/crackcomm/go-actions/source"

var testNotStore = "http://127.0.0.1:3000/api/v1/my/bucket/"
var testStore = testNotStore + "actions/"

// in these tests timer is used to follow the order - add, get, delete

func TestSourceAdd(t *T) {
	Convey("Source.Add()", t, func() {
		s := &Source{Address: testStore}
		Convey("It should add new action", func() {
			err := s.Add("test.new", &Action{Name: "test.new"})
			So(err, ShouldEqual, nil)
		})
		Convey("It should not add action with empty name", func() {
			err := s.Add("", &Action{Name: "test.new"})
			So(err, ShouldEqual, source.ErrEmptyName)
		})
		Convey("It should report error when bad path was set", func() {
			s := &Source{Address: "!"}
			err := s.Add("test.new", &Action{Name: "test.new"})
			So(err, ShouldNotEqual, nil)
		})
	})
}

func TestSourceAll(t *T) {
	<-time.After(200 * time.Millisecond)
	Convey("Source.All()", t, func() {
		s := &Source{Address: testStore}
		Convey("It should find one action", func() {
			a, err := s.All()
			So(err, ShouldEqual, nil)
			So(len(a), ShouldEqual, 1)
		})
		Convey("It should report error when bad path was set", func() {
			s := &Source{Address: "!"}
			_, err := s.All()
			So(err, ShouldNotEqual, nil)
		})
		Convey("It should report error not found when bad path was requested", func() {
			s := &Source{Address: testNotStore}
			_, err := s.All()
			So(err, ShouldEqual, source.ErrNotFound)
		})
	})
}

func TestSourceGet(t *T) {
	<-time.After(400 * time.Millisecond)
	Convey("Source.Get()", t, func() {
		s := &Source{Address: testStore}
		Convey("It should find test action", func() {
			a, err := s.Get("test.new")
			So(err, ShouldEqual, nil)
			So(a.Name, ShouldEqual, "test.new")
		})
		Convey("It should not request empty action name", func() {
			_, err := s.Get("")
			So(err, ShouldEqual, source.ErrEmptyName)
		})
		Convey("It should not find non existent action", func() {
			a, err := s.Get("not.found")
			So(err, ShouldEqual, source.ErrNotFound)
			So(a, ShouldEqual, nil)
		})
		Convey("It should report error when bad path was set", func() {
			s := &Source{Address: "!"}
			_, err := s.Get("/")
			So(err, ShouldNotEqual, nil)
		})
	})
}

func TestSourceDelete(t *T) {
	<-time.After(600 * time.Millisecond)
	Convey("Source.Delete()", t, func() {
		s := &Source{Address: testStore}
		Convey("It should delete test action", func() {
			err := s.Delete("test.new")
			So(err, ShouldEqual, nil)
		})
		Convey("It should not request deletion with empty action name", func() {
			err := s.Delete("")
			So(err, ShouldEqual, source.ErrEmptyName)
		})
		Convey("It should report error when bad path was set", func() {
			s := &Source{Address: "!"}
			err := s.Delete("/")
			So(err, ShouldNotEqual, nil)
		})
	})
}

func TestSourceSendRequest(t *T) {
	Convey("Source.sendRequest()", t, func() {
		Convey("It should not send request with empty transport", func() {
			s := &Source{Address: "!", Authorization: "token"}
			_, err := s.sendRequest("", "/", nil)
			So(err.Error(), ShouldEqual, `unsupported protocol transport ""`)
		})
	})
}

func TestSourceURL(t *T) {
	Convey("Source.url()", t, func() {
		s := &Source{Address: "!"}
		Convey("It should return empty name error when empty name is set", func() {
			_, err := s.url("")
			So(err, ShouldEqual, source.ErrEmptyName)
		})
	})
}

func TestReadStatusError(t *T) {
	Convey("readStatusError()", t, func() {
		Convey("When 500 occurs it should return Server Error", func() {
			err := readStatusError(http.StatusInternalServerError)
			So(err, ShouldEqual, ErrServerError)
		})
		Convey("When 404 occurs it should return Not Found Error", func() {
			err := readStatusError(http.StatusNotFound)
			So(err, ShouldEqual, source.ErrNotFound)
		})
	})
}
