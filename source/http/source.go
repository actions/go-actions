// Package http defines http action source.
//
// It registers http source in github.com/crackcomm/go-actions/source.
package http

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strings"

	"github.com/crackcomm/go-actions/action"
	"github.com/crackcomm/go-actions/source"
)

// ErrServerError - Error returned when response status was 500.
var ErrServerError = errors.New("Unexpected server error")

// contentType - Content-Type used to transport actions.
var contentType = "application/json"

// Source - HTTP actions source.
type Source struct {
	Addr string
	Auth string // Authorization header
}

// All - Requests all available actions.
func (s *Source) All() (res action.Actions, err error) {
	// Send request
	response, err := s.sendRequest("GET", s.Addr, nil)
	if err != nil {
		return
	}
	defer response.Body.Close()

	// Read error from response
	err = readStatusError(response.StatusCode)
	if err != nil {
		return
	}

	// Read action from response
	res = make(action.Actions)
	err = json.NewDecoder(response.Body).Decode(&res)
	return
}

// Add - Adds action to http store. Creates a POST request to action URL with action in body.
func (s *Source) Add(name string, a *action.Action) (err error) {
	// Get action URL
	uri, err := s.url(name)
	if err != nil {
		return
	}

	// Encode action to JSON
	body, err := marshalJSON(a)
	if err != nil {
		return
	}

	// Send request
	response, err := s.sendRequest("POST", uri, body)
	if err != nil {
		return
	}
	defer response.Body.Close()

	// Read error from response
	err = readStatusError(response.StatusCode)
	return
}

// Get - Gets action from http source. Creates a GET request to action URL.
func (s *Source) Get(name string) (a *action.Action, err error) {
	// Get action URL
	uri, err := s.url(name)
	if err != nil {
		return
	}

	// Send request
	response, err := s.sendRequest("GET", uri, nil)
	if err != nil {
		return
	}
	defer response.Body.Close()

	// Read error from response
	err = readStatusError(response.StatusCode)
	if err != nil {
		return nil, err
	}

	// Read action from response
	a = &action.Action{}
	err = json.NewDecoder(response.Body).Decode(a)

	return
}

// Delete - Deletes action from http source. Creates a DELETE request to action URL.
func (s *Source) Delete(name string) (err error) {
	// Get action URL
	uri, err := s.url(name)
	if err != nil {
		return
	}

	// Send request
	response, err := s.sendRequest("DELETE", uri, nil)
	if err != nil {
		return
	}
	defer response.Body.Close()

	// Read error from response
	err = readStatusError(response.StatusCode)
	return
}

func (s *Source) sendRequest(method, uri string, body io.Reader) (response *http.Response, err error) {
	// Create a HTTP Request
	req, err := http.NewRequest(method, uri, body)
	if err != nil {
		return
	}

	// Set Content-Type if body is not empty
	if body != nil {
		req.Header.Set("Content-Type", contentType)
	}

	// Authorize request
	if len(s.Auth) > 0 {
		req.Header.Set("Authorization", s.Auth)
	}

	// Send request
	response, err = http.DefaultClient.Do(req)
	return
}

// url - Creates URL to action from `s.Addr` and name.
func (s *Source) url(name string) (uri string, err error) {
	if len(name) == 0 {
		err = source.ErrEmptyName
		return
	}

	// Append / to s.Addr
	if last := s.Addr[len(s.Addr)-1]; last != '/' {
		s.Addr = s.Addr + "/"
	}

	// Create uri from `s.Addr` and `{name}.json`
	uri = s.Addr + name

	return
}

// readStatusError - Reads response status code and returns error if any.
func readStatusError(status int) (err error) {
	// If response status code was StatusInternalServerError - return ErrServerError
	if status == http.StatusInternalServerError {
		err = ErrServerError
		return
	}

	// If response status code was StatusNotFound - return ErrNotFound
	if status == http.StatusNotFound {
		err = source.ErrNotFound
	}

	return
}

// marshalJSON - Marshals interface to json and returns a buffer.
func marshalJSON(v interface{}) (io.Reader, error) {
	buffer := &bytes.Buffer{}
	err := json.NewEncoder(buffer).Encode(v)
	return buffer, err
}

func init() {
	source.Register("http", func(address string, authorization ...string) (source.Source, error) {
		auth := base64.RawURLEncoding.EncodeToString([]byte(strings.Join(authorization, ":")))
		return &Source{Addr: "http://" + address, Auth: auth}, nil
	})
	source.Register("https", func(address string, authorization ...string) (source.Source, error) {
		auth := base64.RawURLEncoding.EncodeToString([]byte(strings.Join(authorization, ":")))
		return &Source{Addr: "https://" + address, Auth: auth}, nil
	})
}
