package source

import "errors"
import . "testing"
import . "github.com/crackcomm/go-actions/action"
import . "github.com/smartystreets/goconvey/convey"

// testSource - empty source for test purposes
type testSource struct{}

var testerr = errors.New("test error")

func (s *testSource) Delete(name string) (err error) { return }
func (s *testSource) All() (actions Actions, err error) {
	err = testerr
	return
}
func (s *testSource) Add(name string, a *Action) (err error) {
	err = testerr
	return
}
func (s *testSource) Get(name string) (a *Action, err error) {
	a = &Action{Name: name}
	return
}

func TestNewSources(t *T) {
	Convey("NewSources", t, func() {
		Convey("It should create empty sources list", func() {
			l := NewSources()
			So(l.Len(), ShouldEqual, 0)
		})

		Convey("It should create sources list with one element", func() {
			l := NewSources(&testSource{})
			So(l.Len(), ShouldEqual, 1)
		})
	})
}

func TestSourcesLen(t *T) {
	Convey("Sources.Len()", t, func() {
		Convey("It should return sources list length", func() {
			l := NewSources(&testSource{})
			So(l.Len(), ShouldEqual, 1)
		})
	})
}

func TestSourcesClone(t *T) {
	Convey("Sources.Clone()", t, func() {
		Convey("It should clone sources", func() {
			l := NewSources(&testSource{})
			c := l.Clone()
			So(c, ShouldNotEqual, l)
			So(c.Len(), ShouldEqual, 1)
		})
	})
}

func TestSourcesMore(t *T) {
	Convey("Sources.More()", t, func() {
		Convey("It should add two sources", func() {
			l := NewSources()
			l.More(&testSource{})
			l.More(&testSource{})

			// Check
			So(l.Len(), ShouldEqual, 2)
			So(l.List[0], ShouldNotEqual, nil)
			So(l.List[1], ShouldNotEqual, nil)
		})
	})
}

func TestSourcesAdd(t *T) {
	Convey("Sources.Add()", t, func() {
		test := &Action{Name: "ok"}
		Convey("It should report error when error occured", func() {
			l := NewSources(new(testSource))
			err := l.Add("test", test)
			So(err, ShouldEqual, testerr)
		})
		Convey("It should add action", func() {
			src, err := Create("mem://")
			So(err, ShouldEqual, nil)
			l := NewSources(src)
			err = l.Add("test.action", test)
			So(err, ShouldEqual, nil)

			// Check with get
			a, err := l.Get("test.action")
			So(err, ShouldEqual, nil)
			So(test, ShouldEqual, a)
		})
	})
}

func TestSourcesAll(t *T) {
	Convey("Sources.All()", t, func() {
		Convey("It should report error when error occured", func() {
			l := NewSources(new(testSource))
			_, err := l.All()
			So(err, ShouldEqual, testerr)
		})
		Convey("It should return all actions", func() {
			l := NewSources(NewActions())
			test := &Action{Name: "ok"}
			err := l.Add("test.action", test)
			So(err, ShouldEqual, nil)

			// Check all
			a, err := l.All()
			So(err, ShouldEqual, nil)
			So(len(a), ShouldEqual, 1)
			So(a["test.action"], ShouldEqual, test)
		})
	})
}

func TestSourcesGet(t *T) {
	Convey("Sources.Get()", t, func() {
		Convey("It should find action", func() {
			l := NewSources(&testSource{})
			a, err := l.Get("test.action")

			// Check
			So(a, ShouldNotEqual, nil)
			So(err, ShouldEqual, nil)
			So(a.Name, ShouldEqual, "test.action")
		})

		Convey("It shouldn't find any action", func() {
			l := NewSources()
			a, err := l.Get("test.action")

			// Check
			So(a, ShouldEqual, nil)
			So(err, ShouldNotEqual, nil)
			So(err.Error(), ShouldEqual, "Action test.action was not found")
		})
	})
}
