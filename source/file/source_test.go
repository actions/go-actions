package file

import "os"
import "time"
import . "testing"
import . "github.com/crackcomm/go-actions/action"
import . "github.com/smartystreets/goconvey/convey"
import "github.com/crackcomm/go-actions/source"

var testDir = os.TempDir()

// in these tests timer is used to follow the order - add, get, delete

func TestSourceJSON(t *T) {
	Convey("Source.Add()", t, func() {
		s := &Source{testDir}
		Convey("It should add new action", func() {
			err := s.Add("jsontest.new", &Action{Name: "test.new"})
			So(err, ShouldEqual, nil)
		})
	})
	Convey("Source.All()", t, func() {
		s := &Source{testDir}
		Convey("It should list all actions", func() {
			all, err := s.All()
			So(len(all), ShouldEqual, 1)
			So(err, ShouldEqual, nil)
			So(all["jsontest.new"].Name, ShouldEqual, "test.new")
		})
	})
	Convey("Source.Get()", t, func() {
		<-time.After(100 * time.Millisecond)
		s := &Source{testDir}
		Convey("It should find test action", func() {
			a, err := s.Get("jsontest.new")
			So(err, ShouldEqual, nil)
			So(a.Name, ShouldEqual, "test.new")
		})
		Convey("It should not find non existent action", func() {
			a, err := s.Get("not.found")
			So(a, ShouldEqual, nil)
			So(err, ShouldEqual, source.ErrNotFound)
		})
	})
	Convey("Source.Delete()", t, func() {
		<-time.After(200 * time.Millisecond)
		s := &Source{testDir}
		Convey("It should delete test action", func() {
			err := s.Delete("jsontest.new")
			So(err, ShouldEqual, nil)
		})
	})
}

func TestSourceYAML(t *T) {
	Convey("Source.Add()", t, func() {
		DefaultExtension = ".yaml"
		<-time.After(300 * time.Millisecond)
		s := &Source{testDir}
		Convey("It should add new action", func() {
			err := s.Add("yamltest.new", &Action{Name: "test.new"})
			So(err, ShouldEqual, nil)
		})
	})
	Convey("Source.Get()", t, func() {
		<-time.After(400 * time.Millisecond)
		s := &Source{testDir}
		Convey("It should find test action", func() {
			a, err := s.Get("yamltest.new")
			So(err, ShouldEqual, nil)
			So(a.Name, ShouldEqual, "test.new")
		})
	})
	Convey("Source.Delete()", t, func() {
		<-time.After(500 * time.Millisecond)
		s := &Source{testDir}
		Convey("It should delete test action", func() {
			err := s.Delete("yamltest.new")
			So(err, ShouldEqual, nil)
		})
	})
}
