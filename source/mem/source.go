// Package mem defines mem action source.
//
// It registers mem source in github.com/crackcomm/go-actions/source.
package mem

import "github.com/crackcomm/go-actions/action"
import "github.com/crackcomm/go-actions/source"

func init() {
	source.Register("mem", func(_ string, _ ...string) (source.Source, error) {
		return action.NewActions(), nil
	})
}
