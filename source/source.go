// Package source defines source interface.
package source

import "fmt"
import "strings"
import "github.com/crackcomm/go-actions/action"

// Source - Source interface.
type Source interface {
	All() (action.Actions, error)
	Add(name string, a *action.Action) error
	Get(name string) (*action.Action, error)
	Delete(name string) error
}

// Constructor - Constructs a new actions source.
type Constructor func(address string, authorization ...string) (Source, error)

// Constructors - Clients constructors by transport.
var Constructors = make(map[string]Constructor)

// Create - Constructs a new actions source by a transport contained in address.
func Create(address string, authorization ...string) (Source, error) {
	addr := strings.Split(address, "://")
	if len(addr) < 2 {
		return nil, fmt.Errorf("Wrong address")
	}
	scheme, address := addr[0], addr[1]
	if constructor := Constructors[scheme]; constructor != nil {
		return constructor(address, authorization...)
	}
	return nil, fmt.Errorf("Action source constructor for transport %#v was not found", scheme)
}

// Register - Registers a new client constructor
func Register(transport string, constructor Constructor) {
	Constructors[transport] = constructor
}
