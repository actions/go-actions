package source

import . "testing"
import . "github.com/crackcomm/go-actions/action"
import . "github.com/smartystreets/goconvey/convey"

func TestCreate(t *T) {
	Convey("Create", t, func() {
		Convey("It should create a memory source", func() {
			src, err := Create("mem://db")
			So(err, ShouldEqual, nil)
			_, ok := src.(Actions)
			So(ok, ShouldEqual, true)
		})
		Convey("It should report error when no constructor was found", func() {
			_, err := Create("nononono://nonono")
			So(err.Error(), ShouldEqual, `Action source constructor for transport "nononono" was not found`)
		})
	})
}

func init() {
	Register("mem", func(_ string, _ ...string) (Source, error) {
		return NewActions(), nil
	})
}
