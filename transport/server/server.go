// Package server defines methods for constructing servers.
package server

import "fmt"
import "strings"
import "github.com/crackcomm/go-actions/runner"

// Server - Runners server interface.
type Server interface {
	Start() error // starts a server
	Close() error // closes a server
}

// Constructor - Function that creates a runner from arguments.
type Constructor func(address string, r runner.Runner, auth ...string) (Server, error)

// Constructors - List of server constructors by transport.
var Constructors = make(map[string]Constructor)

// Create - Creates a new server from default constructors by transport contained in address.
func Create(address string, r runner.Runner, auth ...string) (Server, error) {
	addr := strings.Split(address, "://")
	if len(addr) < 2 {
		return nil, fmt.Errorf("Wrong address")
	}
	scheme, address := addr[0], addr[1]
	if constructor := Constructors[scheme]; constructor != nil {
		return constructor(address, r, auth...)
	}
	return nil, fmt.Errorf("Server constructor for transport %#v was not found", scheme)
}

// Register - Registers a new server constructor to default constructors.
func Register(transport string, constructor Constructor) {
	Constructors[transport] = constructor
}
