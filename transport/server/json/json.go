// Package json defines json runner server.
//
// It registers json server in github.com/crackcomm/go-actions/transport/server.
package json

import "fmt"
import "net"
import "encoding/json"
import "github.com/crackcomm/go-actions/action"
import "github.com/crackcomm/go-actions/runner"
import "github.com/crackcomm/go-actions/transport/server"

// Server - Server running actions through HTTP.
type Server struct {
	Runner   runner.Runner
	Addr     string
	certFile string // ssl cert
	keyFile  string // ssl key
	ln       net.Listener
	closed   bool
}

// NewServer - Creates a new jsonrpc server.
func NewServer(r runner.Runner) *Server {
	return &Server{Runner: r}
}

// NewServerTLS - Creates a new jsonrpc TLS server.
func NewServerTLS(certFile, keyFile string, r runner.Runner) *Server {
	srv := NewServer(r)
	srv.certFile = certFile
	srv.keyFile = keyFile
	return srv
}

// Start - Starts a server.
func (srv *Server) Start() error {
	// If cert and key are set -- start TLS server
	// if srv.certFile != "" && srv.keyFile != "" {
	// 	return srv.listenAndServeTLS(srv.certFile, srv.keyFile)
	// }
	return srv.listenAndServe()
}

// listenAndServe - Starts listener and serves run requests.
func (srv *Server) listenAndServe() (err error) {
	// Start a server
	srv.ln, err = srv.listen()
	if err != nil {
		return
	}

	// serve connections
	srv.serve()
	return
}

func writeError(conn net.Conn, err error) {
	conn.Write([]byte(fmt.Sprintf(`{"error":%#v}`, err.Error())))
}

// serveconn - serves connection.
func (srv *Server) serveconn(conn net.Conn) {
	defer conn.Close()
	for {
		var err error = nil
		if srv.closed {
			break
		}
		a := &action.Action{}
		decoder := json.NewDecoder(conn)
		decoder.UseNumber()
		err = decoder.Decode(a)
		if err != nil {
			writeError(conn, err)
			continue
		}

		// If action name is close -> close connection
		if a.Name == "close" {
			return
		}

		// run action
		result, err := srv.Runner.Run(a)
		if err != nil {
			writeError(conn, err)
			continue
		}
		defer result.Close()

		// write result
		json.NewEncoder(conn).Encode(result)
	}
}

// serve - serves connections.
func (srv *Server) serve() {
	for {
		// wait for connection
		conn, err := srv.ln.Accept()
		if err == nil {
			// Accept blocks so we have to terminate connection after acception
			if srv.closed {
				conn.Close()
				return
			}
			// go serve connection
			go srv.serveconn(conn)
		}
	}
}

// listen - Starts a tcp listener.
func (srv *Server) listen() (ln net.Listener, err error) {
	ln, err = net.Listen("tcp", srv.Addr)
	if err == nil {
		srv.ln = ln
	}
	return
}

// Close - Closes a server. Not implemented.
func (srv *Server) Close() error {
	srv.closed = true
	if srv.ln == nil {
		return nil
	}
	return srv.ln.Close()
}

func init() {
	server.Register("json", func(address string, r runner.Runner, auth ...string) (server.Server, error) {
		srv := NewServer(r)
		srv.Addr = address
		if len(auth) >= 2 {
			srv.certFile = auth[0]
			srv.keyFile = auth[1]
		}
		return srv, nil
	})
}
