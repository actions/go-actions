// Package http defines server for JSON actions through HTTP.
//
// It registers http server in github.com/crackcomm/go-actions/transport/server.
package http

import "fmt"
import "time"
import "net/http"
import "encoding/json"
import "github.com/golang/glog"
import "github.com/crackcomm/go-actions/action"
import "github.com/crackcomm/go-actions/runner"
import "github.com/crackcomm/go-actions/transport/server"

// Server - Server running actions through HTTP.
type Server struct {
	Server   *http.Server
	Runner   runner.Runner
	certFile string // ssl cert
	keyFile  string // ssl key
}

var serverError = `{"error": %#v, "code": 500}`
var emptyNameError = `{"error": "Action name can not be empty", "code": 400}`
var badRequestError = `{"error": "Bad request", "code": 400}`

// NewServer - Creates a new HTTP server.
func NewServer(r runner.Runner) *Server {
	handler := http.NewServeMux()
	srv := &Server{
		Runner: r,
		Server: &http.Server{
			MaxHeaderBytes: 1 << 20,
			WriteTimeout:   30 * time.Second,
			ReadTimeout:    30 * time.Second,
			Handler:        handler,
		},
	}

	// Enable Keep-Alive
	srv.Server.SetKeepAlivesEnabled(true)

	// Add handler
	handler.Handle("/", srv)
	return srv
}

// NewServerTLS - Creates a new HTTP/TLS server.
func NewServerTLS(certFile, keyFile string, r runner.Runner) *Server {
	srv := NewServer(r)
	srv.certFile = certFile
	srv.keyFile = keyFile
	return srv
}

// Start - Starts a server.
func (srv *Server) Start() error {
	// If cert and key are set -- start TLS server
	if srv.certFile != "" && srv.keyFile != "" {
		return srv.Server.ListenAndServeTLS(srv.certFile, srv.keyFile)
	}
	return srv.Server.ListenAndServe()
}

var runRoute = "/run"
var runRouteLen = len(runRoute)

// ServeHTTP - Serves a HTTP request.
func (srv *Server) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	// Abort if path is not /run
	if len(req.URL.Path) < runRouteLen || req.URL.Path[:runRouteLen] != runRoute {
		w.Write([]byte(`OK`))
		return
	}

	// Write response content-type
	w.Header().Add("Content-Type", "application/json")

	// Read JSON action from request if any
	var err error
	a := &action.Action{Ctx: make(action.Map)}

	// If action name is set in route -- read request as action context
	if len(req.URL.Path) > runRouteLen+1 {
		if req.Method == "POST" {
			a.Name = req.URL.Path[runRouteLen+1:]
			if req.Header.Get("Content-Type") == "application/json" {
				err = json.NewDecoder(req.Body).Decode(&a.Ctx)
			}
		}
	} else {
		err = json.NewDecoder(req.Body).Decode(a)
	}

	// Check error returned when reading request
	if err != nil {
		glog.V(3).Infof("request read error %v", err)
		http.Error(w, badRequestError, http.StatusBadRequest)
		return
	}

	// Read url query to context values
	if req.Method == "GET" {
		for key, values := range req.URL.Query() {
			if len(values) == 0 {
			} else if len(values) == 1 {
				a.Ctx.Add(key, values[1])
			} else if len(values) >= 1 {
				a.Ctx.Add(key, values)
			} else {
				a.Ctx.Add(key, true) // If no values but key is set -- treat as true bool
			}
		}
	}

	glog.V(3).Infof("run %s", a.Name)

	// Run action
	result, err := srv.Runner.Run(a)
	if err != nil {
		http.Error(w, fmt.Sprintf(serverError, err), http.StatusInternalServerError)
		return
	}
	defer result.Close()

	json.NewEncoder(w).Encode(result)
}

// Close - Closes a server. Not implemented.
func (srv *Server) Close() error {
	return nil
}

func init() {
	server.Register("http", func(address string, r runner.Runner, auth ...string) (server.Server, error) {
		srv := NewServer(r)
		srv.Server.Addr = address
		if len(auth) >= 2 {
			srv.certFile = auth[0]
			srv.keyFile = auth[1]
		}
		return srv, nil
	})
}
