package http

import . "testing"
import . "github.com/smartystreets/goconvey/convey"
import "github.com/crackcomm/go-actions/transport/server"

func TestServer(t *T) {
	Convey("server", t, func() {
		Convey("Create()", func() {
			Convey("It should create a new http server", func() {
				s, err := server.Create("http://127.0.0.1:6060", nil)
				So(err, ShouldEqual, nil)
				js, ok := s.(*Server)
				So(ok, ShouldEqual, true)
				So(js.Server.Addr, ShouldEqual, "127.0.0.1:6060")
			})
		})
	})
}
