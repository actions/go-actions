// Package client defines methods for constructing clients.
package client

import "fmt"
import "strings"
import "github.com/crackcomm/go-actions/runner"

// Constructor - Constructs a runner from function.
type Constructor func(address string, authorization ...string) (runner.Runner, error)

// Constructors - Clients constructors by transport.
var Constructors = make(map[string]Constructor)

// Create - Constructs a new client by a transport from default constructors.
func Create(address string, authorization ...string) (runner.Runner, error) {
	addr := strings.Split(address, "://")
	if len(addr) < 2 {
		return nil, fmt.Errorf("Wrong address")
	}
	scheme, address := addr[0], addr[1]
	if constructor := Constructors[scheme]; constructor != nil {
		return constructor(address, authorization...)
	}
	return nil, fmt.Errorf("Client constructor for transport %#v was not found", scheme)
}

// Register - Registers a new client constructor to default constructors.
func Register(transport string, constructor Constructor) {
	Constructors[transport] = constructor
}
