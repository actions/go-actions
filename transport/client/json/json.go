// Package json defines action runner through tcp/json.
//
// It registers json client in github.com/crackcomm/go-actions/transport/client.
//
// TODO: connection pooling (on call)
package json

import "net"
import "encoding/json"
import "github.com/crackcomm/go-actions/action"
import "github.com/crackcomm/go-actions/runner"
import "github.com/crackcomm/go-actions/transport/client"

// Client - TCP/JSON action runner client.
type Client struct {
	Connection net.Conn
	Auth       []string
	Addr       string
}

// Run - Runs action through tcp/json.
func (c *Client) Run(a *action.Action) (result action.Map, err error) {
	if c.Connection == nil {
		err = c.connect()
		if err != nil {
			return
		}
	}

	// Write action
	err = json.NewEncoder(c.Connection).Encode(a)
	if err != nil {
		return
	}

	// Read response
	result = make(action.Map)
	err = json.NewDecoder(c.Connection).Decode(&result)
	return
}

// Call - Calls action through tcp/json.
func (c *Client) Call(name string, contexts ...action.Map) (result action.Map, err error) {
	return c.Run(&action.Action{Name: name, Ctx: action.Flat(contexts...)})
}

// connect - establishes a tcp connection
func (c *Client) connect() (err error) {
	c.Connection, err = net.Dial("tcp", c.Addr)
	return
}

func init() {
	client.Register("json", func(address string, auth ...string) (runner.Runner, error) {
		return &Client{Addr: address, Auth: auth}, nil
	})
}
