package json

import . "testing"
import . "github.com/smartystreets/goconvey/convey"
import "github.com/crackcomm/go-actions/transport/client"

func TestClient(t *T) {
	Convey("client", t, func() {
		Convey("Create()", func() {
			Convey("It should create a new json client", func() {
				s, err := client.Create("json://127.0.0.1:6060", "username", "password")
				So(err, ShouldEqual, nil)
				js, ok := s.(*Client)
				So(ok, ShouldEqual, true)
				So(js.Addr, ShouldEqual, "127.0.0.1:6060")
				So(js.Auth[0], ShouldEqual, "username")
				So(js.Auth[1], ShouldEqual, "password")
			})
		})
	})
}
