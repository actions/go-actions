package http

import . "testing"
import . "github.com/smartystreets/goconvey/convey"
import "github.com/crackcomm/go-actions/transport/client"

func TestClient(t *T) {
	Convey("client", t, func() {
		Convey("Create(http)", func() {
			Convey("It should create a new http client", func() {
				s, err := client.Create("http://127.0.0.1:6060", "username", "password")
				So(err, ShouldEqual, nil)
				js, ok := s.(*Client)
				So(ok, ShouldEqual, true)
				So(js.Addr, ShouldEqual, "http://127.0.0.1:6060")
				So(js.Auth, ShouldEqual, "dXNlcm5hbWU6cGFzc3dvcmQ=")
			})
		})
		Convey("Create(https)", func() {
			Convey("It should create a new http client", func() {
				s, err := client.Create("https://127.0.0.1:6060", "username", "password")
				So(err, ShouldEqual, nil)
				js, ok := s.(*Client)
				So(ok, ShouldEqual, true)
				So(js.Addr, ShouldEqual, "https://127.0.0.1:6060")
				So(js.Auth, ShouldEqual, "dXNlcm5hbWU6cGFzc3dvcmQ=")
			})
		})
	})
}
