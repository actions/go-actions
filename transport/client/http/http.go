// Package http defines client for JSON actions through HTTP.
//
// To crea
//
// It registers http client in github.com/crackcomm/go-actions/transport/client.
package http

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/crackcomm/go-actions/action"
	"github.com/crackcomm/go-actions/runner"
	"github.com/crackcomm/go-actions/transport/client"
)

// Client - HTTP actions runner client.
type Client struct {
	Addr string
	Auth string
}

// Run - Runs action through http.
func (c *Client) Run(a *action.Action) (result action.Map, err error) {
	return c.post("/run", a)
}

// Call - Calls action through http.
func (c *Client) Call(name string, contexts ...action.Map) (result action.Map, err error) {
	return c.post("/run/"+name, action.Flat(contexts...))
}

// post - Creates authenticated HTTP POST call to client address + path. Writes and reads JSON.
func (c *Client) post(path string, value interface{}) (result action.Map, err error) {
	// Marshal request
	buffer := new(bytes.Buffer)
	err = json.NewEncoder(buffer).Encode(value)
	if err != nil {
		return
	}

	// Create a new request
	req, err := http.NewRequest("POST", c.Addr+path, buffer)
	if err != nil {
		return
	}

	// Add Content-Type header
	req.Header.Add("Content-Type", "application/json")
	if len(c.Auth) > 0 {
		req.Header.Add("Authorization", c.Auth)
	}

	// Send request to {c.Addr}/run/{name}
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	defer response.Body.Close()

	// Read action from response
	result = make(action.Map)
	err = json.NewDecoder(response.Body).Decode(&result)
	if result["error"] != nil {
		err = fmt.Errorf("%s", result["error"])
		return
	}
	return
}

func init() {
	client.Register("http", func(address string, authorization ...string) (runner.Runner, error) {
		auth := base64.RawURLEncoding.EncodeToString([]byte(strings.Join(authorization, ":")))
		return &Client{Addr: "http://" + address, Auth: auth}, nil
	})
	client.Register("https", func(address string, authorization ...string) (runner.Runner, error) {
		auth := base64.RawURLEncoding.EncodeToString([]byte(strings.Join(authorization, ":")))
		return &Client{Addr: "https://" + address, Auth: auth}, nil
	})
}
