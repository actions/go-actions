package client

import . "testing"
import . "github.com/smartystreets/goconvey/convey"
import "github.com/crackcomm/go-actions/runner"

func testConstructor(_ string, _ ...string) (runner.Runner, error) {
	return nil, nil
}

func TestClient(t *T) {
	Convey("Client", t, func() {
		Convey("Create()", func() {
			Convey("It should create new client", func() {
				Register("test", testConstructor)
				a, b := Create("test://123.0.0.1")
				So(a, ShouldEqual, nil)
				So(b, ShouldEqual, nil)
			})
			Convey("It should return error when constructor was not found", func() {
				_, err := Create("chickit://123.0.0.1")
				So(err.Error(), ShouldEqual, `Client constructor for transport "chickit" was not found`)
			})
		})
		Convey("Register()", func() {
			Convey("It should register a new constructor", func() {
				Register("httpziom", testConstructor)
				So(Constructors["httpziom"], ShouldEqual, testConstructor)
			})
		})
	})
}
