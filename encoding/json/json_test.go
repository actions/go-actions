package json

import "fmt"
import "bytes"
import . "testing"
import . "github.com/crackcomm/go-actions/action"
import . "github.com/smartystreets/goconvey/convey"

var actionJSON = `{
	"name": "http.request",
	"ctx": {
		"url": "http://google.com/"
	},
	"next": {
		"name": "html.extract",
		"ctx": {
			"selectors": {"title": "title"}
		}
	}
}`

var mapJSON = fmt.Sprintf(`{"google.title": %s}`, actionJSON)

var googleTitle = &Action{
	Name: "http.request",
	Ctx: Map{
		"url": "http://google.com/",
	},
	Next: &Action{
		Name: "html.extract",
		Ctx: Map{
			"selectors": Map{
				"title": "title",
			},
		},
	},
}

var mapExample = map[string]*Action{"google.title": googleTitle}

func TestEncoder(t *T) {
	Convey("Marshal()", t, func() {
		Convey("It should marshal JSON action", func() {
			a := &Action{Name: "some.action"}
			res := new(bytes.Buffer)
			err := NewEncoder(res).Encode(a)
			nonl := string(res.Bytes()[:res.Len()-1]) // without new line
			So(nonl, ShouldEqual, `{"name":"some.action"}`)
			So(err, ShouldEqual, nil)
		})
	})
}

func TestMarshal(t *T) {
	Convey("Marshal()", t, func() {
		Convey("It should marshal JSON action", func() {
			res := `{"name":"some.action"}`
			a, err := Marshal(&Action{Name: "some.action"})
			So(err, ShouldEqual, nil)
			So(res, ShouldEqual, string(a))
		})
	})
}

func TestUnmarshal(t *T) {
	Convey("Unmarshal()", t, func() {
		Convey("It should unmarshal JSON action", func() {
			a, err := Unmarshal([]byte(actionJSON))
			So(a, ShouldNotEqual, nil)
			So(err, ShouldEqual, nil)

			So(a.Name, ShouldEqual, googleTitle.Name)
			u, _ := a.Ctx.Get("url").String()
			So(u, ShouldEqual, "http://google.com/")
			So(a.Next.Name, ShouldEqual, googleTitle.Next.Name)
			m, _ := a.Next.Ctx.Get("selectors").Map()
			t, _ := m.Get("title").String()
			So(t, ShouldEqual, "title")
		})
	})
}

func TestUnmarshalMany(t *T) {
	Convey("UnmarshalMany()", t, func() {
		Convey("It should unmarshal JSON actions map", func() {
			l, err := UnmarshalMany([]byte(mapJSON))
			So(l, ShouldNotEqual, nil)
			So(err, ShouldEqual, nil)

			a := l["google.title"]
			So(a, ShouldNotEqual, nil)

			So(a.Name, ShouldEqual, googleTitle.Name)
			u, _ := a.Ctx.Get("url").String()
			So(u, ShouldEqual, "http://google.com/")
			So(a.Next.Name, ShouldEqual, googleTitle.Next.Name)
			m, _ := a.Next.Ctx.Get("selectors").Map()
			t, _ := m.Get("title").String()
			So(t, ShouldEqual, "title")
		})
	})
}
