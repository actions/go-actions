// Package json implements methods for encoding and decoding json actions.
package json

import "io"
import "bytes"
import "encoding/json"
import "github.com/crackcomm/go-actions/action"

// Decoder - JSON Decoder for actions.
type Decoder struct {
	*json.Decoder
}

// Encoder - JSON Encoder for actions.
type Encoder struct {
	*json.Encoder
}

// NewDecoder - Creates a new JSON decoder.
func NewDecoder(body io.Reader) *Decoder {
	return &Decoder{json.NewDecoder(body)}
}

// NewEncoder - Creates a new JSON encoder.
func NewEncoder(body io.Writer) *Encoder {
	return &Encoder{json.NewEncoder(body)}
}

// Decode - Decodes actions from buffer.
func (m *Decoder) Decode() (a *action.Action, err error) {
	a = new(action.Action)
	err = m.Decoder.Decode(a)
	return
}

// Encode - Encodes actions from buffer.
func (m *Encoder) Encode(a *action.Action) error {
	return m.Encoder.Encode(a)
}

// Marshal - Marshals interface to JSON.
func Marshal(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

// Unmarshal - Unmarshals JSON action.
func Unmarshal(body []byte) (a *action.Action, err error) {
	buffer := bytes.NewBuffer(body)
	a, err = NewDecoder(buffer).Decode()
	return
}
