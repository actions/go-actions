// Package json defines methods for marshaling and unmarshaling JSON actions.
//
// Defined methods can be used to list many actions or just one action.
//
// JSON action
//   {
//     "name": "http.request",
//     "ctx": {
//       "url": "http://www.google.pl/"
//     },
//     "next": {
//       "name": "html.extract",
//       "ctx": {
//         "selectors": {
//           "title": "title"
//         }
//       }
//     }
//   }
//
// To unmarshal JSON action you can use.
//
//  action, err := json.Unmarshal(body)
//
// Unmarshaled can be also a map of actions.
//
// To unmarshal map of JSON actions you can use.
//
//  actions, err := json.UnmarshalMany(body)
//
package json
