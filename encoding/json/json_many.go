// Package json implements methods for encoding and decoding json actions.
package json

import "io"
import "bytes"
import "encoding/json"
import "github.com/crackcomm/go-actions/action"

// ManyDecoder - JSON Decoder for actions.
type ManyDecoder struct {
	*json.Decoder
}

// NewManyDecoder - Creates a new JSON decoder.
func NewManyDecoder(body io.Reader) *ManyDecoder {
	return &ManyDecoder{json.NewDecoder(body)}
}

// Decode - Decodes actions from buffer.
func (m *ManyDecoder) Decode() (a map[string]*action.Action, err error) {
	a = make(map[string]*action.Action)
	err = m.Decoder.Decode(&a)
	return
}

// UnmarshalMany - Unmarshals JSON actions into a Map.
func UnmarshalMany(body []byte) (a map[string]*action.Action, err error) {
	buffer := bytes.NewBuffer(body)
	a, err = NewManyDecoder(buffer).Decode()
	return
}
