package yaml

import "reflect"
import . "testing"
import . "github.com/crackcomm/go-actions/action"
import . "github.com/smartystreets/goconvey/convey"

var listYAML = `
  - http.request:
      url: http://google.com/
  - html.extract:
      list:
        - 1
        - 2
        - 3
      selectors:
        title: title
  - null.action
  -
`

var mapManyYAML = "\ngoogle.title:\n" + listYAML

var listManyYAML = "\n- google.title:\n" + listYAML

var googleTitle = &Action{
	Name: "http.request",
	Ctx: Map{
		"url": "http://google.com/",
	},
	Next: &Action{
		Name: "html.extract",
		Ctx: Map{
			"selectors": Map{
				"title": "title",
			},
			"list": []interface{}{"1", "2", "3"},
		},
		Next: &Action{
			Name: "null.action",
		},
	},
}

var mapExample = map[string]*Action{"google.title": googleTitle}

func TestUnmarshal(t *T) {
	Convey("Unmarshal()", t, func() {
		Convey("It should unmarshal YAML list to action", func() {
			a, err := Unmarshal([]byte(listYAML))
			equal := reflect.DeepEqual(a, googleTitle)
			So(a, ShouldNotEqual, nil)
			So(err, ShouldEqual, nil)
			So(equal, ShouldEqual, true)
		})
		Convey("It should unmarshal action from string", func() {
			a, err := Unmarshal([]byte(`valid yaml !!!`))
			So(a, ShouldNotEqual, nil)
			So(err, ShouldEqual, nil)
			So(a.Name, ShouldEqual, "valid yaml !!!")
		})
		Convey("It should not unmarshal action from invalid yaml", func() {
			a, err := Unmarshal([]byte(`- {test}:
				-[fuck]:`))
			So(a, ShouldEqual, nil)
			So(err, ShouldNotEqual, nil)
		})
	})
}

func TestUnmarshalMany(t *T) {
	Convey("UnmarshalMany()", t, func() {
		Convey("It should unmarshal YAML list to action", func() {
			a, err := UnmarshalMany([]byte(listManyYAML))
			equal := reflect.DeepEqual(a, mapExample)
			So(err, ShouldEqual, nil)
			So(equal, ShouldEqual, true)
		})
		Convey("It should unmarshal YAML map to action", func() {
			a, err := UnmarshalMany([]byte(mapManyYAML))
			equal := reflect.DeepEqual(a, mapExample)
			So(err, ShouldEqual, nil)
			So(equal, ShouldEqual, true)
		})
		Convey("It should not unmarshal action from invalid yaml", func() {
			a, err := UnmarshalMany([]byte(`- {test}:
				-[fsd]:`))
			So(a, ShouldEqual, nil)
			So(err, ShouldNotEqual, nil)
		})
	})
}

func TestMarshal(t *T) {
	Convey("Marshal()", t, func() {
		Convey("Should pretty marshal YAML action", func() {
			expected := "- http.request:\n    url: http://google.com/\n- html.extract:\n    list:\n    - \"1\"\n    - \"2\"\n    - \"3\"\n    selectors:\n      title: title\n- null.action: {}\n"
			b, err := Marshal(googleTitle)

			// Check
			So(err, ShouldEqual, nil)
			So(string(b), ShouldEqual, expected)
		})
	})
}
