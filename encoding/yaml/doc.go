// Package yaml defines methods for marshaling and unmarshaling actions.
//
// Defined methods can be used to list many actions or just one action.
//
// Action can be a YAML map or a list.
//
//  - http.request:
//      url: http://www.google.pl/
//  - html.extract:
//      selectors:
//        title: head > title
//
// JSON equivalent
//   {
//     "name": "http.request",
//     "ctx": {
//       "url": "http://www.google.pl/"
//     },
//     "next": {
//       "name": "html.extract",
//       "ctx": {
//         "selectors": {
//           "title": "title"
//         }
//       }
//     }
//   }
//
// To unmarshal YAML action you can use.
//
//  action, err := yaml.Unmarshal(body)
//
// Unmarshaled can be also a map of action.
//
//   google.title:
//    - http.request:
//        url: http://www.google.pl/
//    - html.extract:
//        selectors:
//          title: head > title
//
// To unmarshal map of YAML actions you can use.
//
//  actions, err := yaml.UnmarshalMany(body)
//
package yaml
