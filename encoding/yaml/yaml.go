package yaml

import "io"
import "bytes"
import yaml2 "gopkg.in/yaml.v1"
import "github.com/crackcomm/go-actions/action"
import "github.com/kylelemons/go-gypsy/yaml"

// Decoder - YAML Decoder.
type Decoder struct {
	io.Reader
}

// Encoder - YAML Encoder.
type Encoder struct {
	io.Writer
}

// NewDecoder - Creates a new Decoder.
func NewDecoder(buffer io.Reader) *Decoder {
	return &Decoder{buffer}
}

// NewEncoder - Creates a new Encoder.
func NewEncoder(buffer io.Writer) *Encoder {
	return &Encoder{buffer}
}

// Decode - Decodes an action from a reader.
func (d *Decoder) Decode() (a *action.Action, err error) {
	node, err := yaml.Parse(d.Reader)
	if err != nil {
		return nil, err
	}
	return nodeToAction(node), nil
}

// Encode - Encodes an action into a writer.
func (e *Encoder) Encode(a *action.Action) (err error) {
	list := formatAction(a)
	body, err := yaml2.Marshal(list)
	if err != nil {
		return
	}
	_, err = e.Writer.Write(body)
	return
}

// Marshal - Marshals YAML action.
func Marshal(a *action.Action) (body []byte, err error) {
	buffer := new(bytes.Buffer)
	err = NewEncoder(buffer).Encode(a)
	if err == nil {
		body = buffer.Bytes()
	}
	return
}

func formatAction(a *action.Action) (list []map[string]action.Map) {
	actionToList(a, &list)
	return
}

func actionToList(a *action.Action, list *[]map[string]action.Map) {
	act := make(map[string]action.Map)
	act[a.Name] = a.Ctx
	*list = append(*list, act)
	if a.Next != nil {
		actionToList(a.Next, list)
	}
}

// Unmarshal - Unmarshals YAML action.
func Unmarshal(body []byte) (*action.Action, error) {
	return NewDecoder(bytes.NewBuffer(body)).Decode()
}

// UnmarshalMany - Unmarshals YAML actions into a Map.
func UnmarshalMany(body []byte) (map[string]*action.Action, error) {
	buffer := bytes.NewBuffer(body)
	node, err := yaml.Parse(buffer)
	if err != nil {
		return nil, err
	}
	return nodeToActions(node), nil
}

// Remember that maps are not key-ordered so it will not work!
// example input:
// http.request:
//   test: ok
// html.extract:
//   ok: true
func mapNodeToAction(node yaml.Map) (result *action.Action) {
	for name, context := range node {
		a := &action.Action{
			Name: name,
			Ctx:  nodeToContext(context),
		}
		if result == nil {
			result = a
		} else {
			result.AddNext(a)
		}
	}
	return
}

// example input:
// - http.request:
//     url: http://www.google.pl/
// - html.extract:
//     selectors:
//       title: head > title
func listNodeToAction(node yaml.List) (result *action.Action) {
	for _, n := range node {
		// Transform node to Action
		a := nodeToAction(n)
		if result == nil {
			result = a
		} else {
			result.AddNext(a)
		}
	}
	return
}

// google.title:
//   - http.request:
//       url: http://www.google.pl/
//   - html.extract:
//       selectors:
//         title: head > title
func mapNodeToActions(node yaml.Map) (result map[string]*action.Action) {
	result = make(map[string]*action.Action)
	for name, n := range node {
		// Transform node to Action
		a := nodeToAction(n)
		if a != nil {
			result[name] = a
		}
	}
	return
}

// - google.title:
//   - http.request:
//       url: http://www.google.pl/
//   - html.extract:
//       selectors:
//         title: head > title
func listNodeToActions(node yaml.List) (result map[string]*action.Action) {
	result = make(map[string]*action.Action)
	for _, n := range node {
		// Transform node to Actions
		if actions := nodeToActions(n); actions != nil {
			for name, a := range actions {
				result[name] = a
			}
		}
	}
	return
}

func nodeValue(node yaml.Node) interface{} {
	switch node.(type) {
	case yaml.Map:
		return nodeToContext(node)
	case yaml.List:
		n, _ := node.(yaml.List)
		return listNodeToList(n)
	case yaml.Scalar:
		if n, ok := node.(yaml.Scalar); ok {
			return n.String()
		}
	}
	return node
}

func listNodeToList(node yaml.List) (result []interface{}) {
	result = []interface{}{}
	for _, n := range node {
		value := nodeValue(n)
		result = append(result, value)
	}
	return
}

func nodeToContext(node yaml.Node) (result action.Map) {
	if node == nil {
		return
	}

	m, ok := node.(yaml.Map)
	if !ok {
		return
	}

	result = make(action.Map)
	for attr, n := range m {
		value := nodeValue(n)
		result.Add(attr, value)
	}
	return
}

func nodeToAction(node yaml.Node) *action.Action {
	switch node.(type) {
	case yaml.List:
		n, _ := node.(yaml.List)
		return listNodeToAction(n)
	case yaml.Map:
		n, _ := node.(yaml.Map)
		return mapNodeToAction(n)
	case yaml.Scalar:
		if n, ok := node.(yaml.Scalar); ok {
			a := &action.Action{
				Name: n.String(),
			}
			return a
		}
	}
	return nil
}

func nodeToActions(node yaml.Node) map[string]*action.Action {
	switch node.(type) {
	case yaml.List:
		n, _ := node.(yaml.List)
		return listNodeToActions(n)
	case yaml.Map:
		n, _ := node.(yaml.Map)
		return mapNodeToActions(n)
	}
	return nil
}
