package local

import "errors"
import . "testing"
import . "github.com/crackcomm/go-actions/action"
import . "github.com/smartystreets/goconvey/convey"
import "github.com/crackcomm/go-actions/core"

// Core functions
import _ "github.com/crackcomm/go-core/html"
import _ "github.com/crackcomm/go-core/http"

var googleAction = &Action{
	Name: "http.request",
	Ctx: Map{
		"url": "http://www.google.com/",
	},
	Next: &Action{
		Name: "html.extract",
		Ctx: Map{
			"selectors": Map{
				"title": "title",
			},
		},
	},
}

func BenchmarkRun(b *B) {
	a := &Action{Name: "noop"}
	for i := 0; i < b.N; i++ {
		DefaultRunner.Core.RawRun(a)
	}
}

func BenchmarkRunNoop(b *B) {
	for i := 0; i < b.N; i++ {
		func() {}()
	}
}

func TestCall(t *T) {
	Convey("Call()", t, func() {
		Convey("It should run action by name", func() {
			res, err := Call("google.title")
			So(err, ShouldEqual, nil)
			So(res["title"], ShouldEqual, "Google")
		})
	})
}

func TestRun(t *T) {
	Convey("Run()", t, func() {
		Convey("It should not run empty action", func() {
			res, err := Run(nil)
			So(res, ShouldEqual, nil)
			So(err, ShouldEqual, core.ErrActionEmpty)
		})

		Convey("It should not run not resolved action", func() {
			res, err := Run(&Action{Name: "not.found"})
			So(res, ShouldEqual, nil)
			So(err.Error(), ShouldEqual, "Action not.found was not found")
		})

		Convey("It should return Google", func() {
			res, err := Run(googleAction)
			So(err, ShouldEqual, nil)
			g, ok := res.Get("title").String()
			So(ok, ShouldEqual, true)
			So(g, ShouldEqual, "Google")
		})

		Convey("It should report fake error", func() {
			testErr := errors.New("test-error")
			DefaultRunner.Core.Add("test.func", &core.Function{Function: func(_ Map) (Map, error) { return nil, testErr }})
			res, err := Run(&Action{Name: "test.func"})
			So(res, ShouldEqual, nil)
			So(err, ShouldEqual, testErr)
		})
	})
}

func init() {
	DefaultRunner.Core.Action("google.title", googleAction)
	DefaultRunner.Core.Add("noop", &core.Function{Function: func(ctx Map) (Map, error) { return ctx, nil }})
}
