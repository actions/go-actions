// Package local defines runner that executes actions using core functions.
package local

import "github.com/crackcomm/go-actions/core"
import "github.com/crackcomm/go-actions/action"

// Runner - Local action runner.
type Runner struct {
	*core.Core
}

// DefaultRunner - Default Runner
var DefaultRunner = &Runner{Core: core.Default}

// Run - Runs action using Default Runner.
func Run(a *action.Action) (action.Map, error) {
	return DefaultRunner.Run(a)
}

// Call - Calls action by name using Default Runner.
func Call(name string, contexts ...action.Map) (action.Map, error) {
	return DefaultRunner.Call(name, contexts...)
}

// Call - Calls action locally.
func (r *Runner) Call(name string, contexts ...action.Map) (ctx action.Map, err error) {
	return r.Core.Call(name, contexts...)
}

// Run - Runs action locally.
func (r *Runner) Run(a *action.Action) (action.Map, error) {
	return r.Core.Run(a)
}
